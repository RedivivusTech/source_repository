﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace HRMSTemplate.ValidationManager
{
    public class ValidationError: IValidator
    {
        public string ErrorMessage { get; set; }
        public bool IsValid { get; set; }

        public ValidationError(string message)
        {
            ErrorMessage = message;
            IsValid = false;
        }

        public void Validate()
        {
            throw new NotImplementedException();
        }
    }
}