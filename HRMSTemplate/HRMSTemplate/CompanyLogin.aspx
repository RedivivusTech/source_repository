﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyLogin.aspx.cs" Inherits="HRMSTemplate.CompanyLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<link rel="stylesheet" href="../../vendors/mdi/css/materialdesignicons.min.css" />--%>
    <link href="template/vendors/mdi/css/materialdesignicons.min.css" rel="stylesheet" />
    <link href="template/vendors/base/vendor.bundle.base.css" rel="stylesheet" />
    <link href="template/css/style.css" rel="stylesheet" />

    <%--<link rel="stylesheet" href="../../vendors/base/vendor.bundle.base.css" />
    <link rel="stylesheet" href="../../css/style.css" />--%>
    <!-- endinject -->
    <link rel="shortcut icon" href="template/images/favicon.png" />
</head>
<body>
    <%--<form id="form1" runat="server">--%>

    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth px-0">
                <div class="row w-100 mx-0">
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                            <div class="brand-logo">
                                <img src="../Images/output-onlinepngtools.png" alt="logo">
                            </div>
                            <h4>Hello! let's get started</h4>
                            <h6 class="font-weight-light">Sign in to continue.</h6>
                            <form class="pt-3" id="form1" runat="server">
                                <div class="form-group">
                                    <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control form-control-lg" TextMode="Email" placeholder="UserName"></asp:TextBox>
                                    <%--<input type="email" class="form-control form-control-lg" id="txtUserName" placeholder="Username" />--%>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txtPass" runat="server" CssClass="form-control form-control-lg" TextMode="Password" placeholder="Password"></asp:TextBox>
                                    <%--<input type="password" class="form-control form-control-lg" id="txtPass" placeholder="Password" />--%>
                                </div>
                                <div class="mt-3">
                                     <asp:Button ID="Button1" runat="server" Text="SIGN IN" OnClick="Button1_Click" CssClass="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" />
                                    <%--<a class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" href="../../index.html" >SIGN IN</a>--%>
                                </div>
                                <div class="my-2 d-flex justify-content-between align-items-center">
                                    <div class="form-check">
                                        <label class="form-check-label text-muted">
                                            <input type="checkbox" class="form-check-input" />
                                            Keep me signed in
                   
                                        </label>
                                    </div>
                                    <a href="#" class="auth-link text-black">Forgot password?</a>
                                </div>
                                <%--<div class="mb-2">
                                    <button type="button" class="btn btn-block btn-facebook auth-form-btn">
                                        <i class="mdi mdi-facebook mr-2"></i>Connect using facebook
                 
                                    </button>
                                </div>--%>
                                <div class="text-center mt-4 font-weight-light">
                                    Don't have an account? <a href="register.html" class="text-primary">Create</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>





    <%--<div>
            <table>
                <tr>
                    <td>UserName:</td>
                    <td>
                        <asp:TextBox ID="txtUserName" runat="server" />
                    </td>
                     
                </tr>
                <tr>
                    <td>Password:</td>
                    <td>
                        <asp:TextBox ID="txtPass" TextMode="Password" runat="server" />
                    </td>
                     
                </tr>
                <tr>
                     <td>
                         <asp:Button ID="Button1" runat="server" Text="Login" OnClick="Button1_Click" />
                     </td>
                    <td>
                        <asp:Button ID="Button2" runat="server" Text="Button" />
                    </td>
                </tr>
            </table>
        </div>--%>
    <%--  </form>--%>

<script src="../template/vendors/base/vendor.bundle.base.js"></script>

  <!-- endinject -->
  <!-- inject:js -->
<script src="../template/js/off-canvas.js"></script>
<script src="../template/js/hoverable-collapse.js"></script>
<script src="../template/js/template.js"></script>


  
</body>
</html>
