﻿using HRMSTemplate.App_Code.BAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace HRMSTemplate
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadBranch();
                LoadClient();
            }
            //DataTable data = clsUserModuleAccess.GetAllModulesByUserIDAndUserRoleID(Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["CompanyID"]));
            //Compamy_tab.Visible = false;
            //Branch_tab.Visible = false;
            //Client_tab.Visible = false;
            //Associate_tab.Visible = false;
            //Shift_tab.Visible = false;
            //Holiday_tab.Visible = false;
            //Dept_tab.Visible = false;
            //Leave_tab.Visible = false;
            //UserRole_tab.Visible = false;
            //User_tab.Visible = false;

            //foreach (DataRow row in data.Rows)
            //{
            //    foreach (var item in row.ItemArray)
            //    {

            //        if (item.ToString() == "Company")
            //            Compamy_tab.Visible = true;
            //        if (item.ToString() == "Branch")
            //            Branch_tab.Visible = true;
            //        if (item.ToString() == "Client")
            //            Client_tab.Visible = true;
            //        if (item.ToString() == "Associate")
            //            Associate_tab.Visible = true;
            //        if (item.ToString() == "Shift")
            //            Shift_tab.Visible = true;
            //        if (item.ToString() == "Holiday")
            //            Holiday_tab.Visible = true;
            //        if (item.ToString() == "Department")
            //            Dept_tab.Visible = true;
            //        if (item.ToString() == "Leave")
            //            Leave_tab.Visible = true;
            //        if (item.ToString() == "UserRole")
            //            UserRole_tab.Visible = true;
            //        if (item.ToString() == "User")
            //            User_tab.Visible = true;
            //    }
            //}


        }

        public void LoadBranch()
        {
            DataSet dataSet = clsBranch.GetBranchNameAndID(Convert.ToInt32(Session["CompanyID"]));
            ddlBranch.DataSource = dataSet;
            ddlBranch.DataTextField = "BranchName";
            ddlBranch.DataValueField = "ID";
            ddlBranch.DataBind();
        }
        public void LoadClient()
        {
            int branchID = (ddlBranch.SelectedValue == "") ? 0 : Convert.ToInt32(ddlBranch.SelectedValue);
            DataSet dataSet = clsClient.GetClientNameAndID(Convert.ToInt32(Session["CompanyID"]), branchID);
            ddlClient.DataSource = dataSet;
            ddlClient.DataTextField = "ClientName";
            ddlClient.DataValueField = "ID";
            ddlClient.DataBind();
        }
        protected void ddlBranch_DataBound(object sender, EventArgs e)
        {
            ddlBranch.Items.Insert(0, new ListItem("--Select Branch--", "0"));
        }

        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadClient();
            Session["BranchID"] = (ddlBranch.SelectedValue == "") ? 0 : Convert.ToInt32(ddlBranch.SelectedValue);
        }

        protected void ddlClient_DataBinding(object sender, EventArgs e)
        {
            ddlClient.Items.Insert(0, new ListItem("--Select Client--", "0"));
        }
    }
}