﻿using HRMSTemplate.App_Code.BAL;
using System;

namespace HRMSTemplate
{
    public partial class CompanyLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string uName = txtUserName.Text;
            string pass = txtPass.Text;
            int companyID = clsCompany.ValidateLogin(uName, pass);

            if (companyID > 0)
            {
                Session["CompanyID"] = companyID;
                Session["UID"] = 1;
                //Session["ClientID"] = 1;
                //Response.Redirect("~/Branch.aspx");
                Response.Redirect("~/Masters/Associate.aspx");

                //Response.Redirect("~/Masters/Client.aspx");
            }
            else
            {
                Response.Write("Invalid UserName and Password");
            }


        }
    }
}