﻿using HRMSTemplate.App_Code.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMSTemplate.Masters
{
    public partial class FinancialYear : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetYears();
            }
        }

        private void GetYears()
        {
            var data = clsFinancialYear.GetAll();
            grdFinancialYear.DataSource = data;
            grdFinancialYear.DataBind();
        }
        protected void btnSaveYear_Click(object sender, EventArgs e)
        {
            clsFinancialYear year = new clsFinancialYear();
            year.ID = (hdnFinancialYearId.Value == "") ? 0 : Convert.ToInt32(hdnFinancialYearId.Value);
            year.FinancialYear = txtYear.Text;
            year.YearStart = DateTime.ParseExact(txtStartDate.Text, "yyyy-MM-dd", null);
            year.YearEnd = DateTime.ParseExact(txtEndDate.Text, "yyyy-MM-dd", null);
            //year.YearEnd = Convert.ToDateTime(txtEndDate.Text);
            int result = clsFinancialYear.SaveFinancialYear(year, Convert.ToInt32(Session["UID"]));
            if (result > 0)
            {
                string msg = null;
                if (year.ID == 0)
                {
                    msg = "Successfully Inserted";
                }
                else
                {
                    msg = "Successfully Updated";
                }
                GetYears();

            }
            else
                Response.Write("Not Inserted");
        }

        protected void EditYear(object sender, EventArgs e)
        {
            clsFinancialYear objYear = new clsFinancialYear();
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            hdnFinancialYearId.Value = ((HiddenField)row.FindControl("hdnyearId")).Value;

            int yearID = Convert.ToInt32(hdnFinancialYearId.Value);
            objYear = clsFinancialYear.GetByID(yearID);

            //hdnBranchID.Value = branchId;
            txtYear.Text = objYear.FinancialYear;
            txtStartDate.Text = objYear.YearStart.ToString("yyyy-MM-dd");
            txtEndDate.Text = objYear.YearEnd.ToString("yyyy-MM-dd");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

        }

        protected void DeleteYear(object sender, EventArgs e)
        {
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            HiddenField hdnYearID = (HiddenField)row.FindControl("hdnyearId");
            int yearID = Convert.ToInt32(hdnYearID.Value);  //stored Branch ID
            int result = clsFinancialYear.RemoveFinancialYear(yearID, Convert.ToInt32(Session["UID"]));
            if (result > 0)
            {
                GetYears();
            }
            else
            {
                Response.Write("Not Deleted");
            }
        }
        protected void AddFinancialYear(object sender, EventArgs e)
        {
            ClearAllFields();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

        }

        private void ClearAllFields()
        {
            txtEndDate.Text = "";
            txtStartDate.Text = "";
            txtYear.Text = "";
        }
    }
}