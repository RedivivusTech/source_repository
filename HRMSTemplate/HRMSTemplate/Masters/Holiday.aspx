﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Holiday.aspx.cs" Inherits="HRMSTemplate.Masters.Holiday" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div>
                        <p class="card-title">
                            Holiday Details
                            <span style="float: right">
                                <asp:Button ID="Button3" runat="server" Text="Add Holiday" CssClass="pull-right mybutton btn btn-primary" CausesValidation="false" OnClick="AddHoliday" />
                            </span>
                        </p>
                    </div>

                    <%--<div class="panel-heading">
                        <div class="row">
                            <h3>Holiday Details</h3>
                            <span>
                                <asp:Button ID="Button1" runat="server" Text="Add Holiday" CssClass="pull-right mybutton btn btn-primary" OnClick="AddHoliday" />
                            </span>
                        </div>
                    </div>--%>
                    <div class="row">
                        <div class="card-body">
                            <div class="table-responsive">

                                <asp:GridView ID="grdHoliday" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Holiday Name">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnHolidayId" runat="server" Value='<%# Eval("ID") %>' />
                                                <asp:Label ID="lblHolidayName" runat="server" Text='<%# Eval("HolidayName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Holiday Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHolidayDate" runat="server" Text='<%# Eval("Holiday") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Year">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFinancialYear" runat="server" Text='<%# Eval("FinYear") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" OnClick="EditHoliday">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkCode" runat="server" OnClientClick="return DeleteHoliday()" OnClick="DeleteHoliday">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Records

                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="holidayModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Holiday</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <%--<asp:HiddenField ID="HiddenField1" runat="server" />--%>
                            <span class="form-control-sm" style="margin-left: -10px;">Client:<span class="text-danger"> *</span></span>
                            <asp:DropDownList ID="ddlClient" runat="server" CssClass="form-control" DataTextField="ClientName" DataValueField="ID" OnDataBound="ddlClient_DataBound" Width="100%"></asp:DropDownList>
                        </div>
                        <div class="col-md-6">
                            <asp:HiddenField ID="hdnHodId" runat="server" />
                            <span class="form-control-sm" style="margin-left: -10px;">Holiday Name:<span class="text-danger"> *</span></span>
                            <asp:TextBox ID="txtHolidayName" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Holiday Date:<span class="text-danger"> *</span></span>
                            <asp:TextBox ID="txtHolidayDate" CssClass="form-control form-control-sm" runat="server" TextMode="Date"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Financial Year:<span class="text-danger"> *</span></span>
                            <asp:TextBox ID="txtYear" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <span>
                        <asp:ValidationSummary ID="validationSummary" ClientIDMode="Static" runat="server" ShowModelStateErrors="true" DisplayMode="List" ForeColor="Red" />
                    </span>
                    <asp:Button ID="btnSaveHoliday" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveHoliday_Click" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        function DeleteHoliday() {
            if (confirm("Are you sure you want to delete this Holiday?")) {
                return true;
            } else {
                return false;
            }
        }
        //$(function () {
        //    $("#txtHolidayDate").datepicker();
        //});

        function ShowPopup() {
            $("#holidayModal").modal("show");
        }
        function ApplyCss() {

            $("#<%=ddlClient.ClientID%>").select2({
                allowClear: true
            });
        }
        $(document).ready(function () {
            ApplyCss();
        });
    </script>
</asp:Content>
