﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Associate.aspx.cs" Inherits="HRMSTemplate.Masters.Associate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div>
                        <p class="card-title">
                            Associate Details
                            <span style="float: right">
                                <asp:Button ID="btnAddAssociate" runat="server" Text="Add Associate" CssClass="btn btn-primary" OnClick="btnAddAssociate_Click" />
                                <%--<input type="button" class="btn btn-primary" onclick="showAssociateModal()" value="Add Associate" />--%></span>
                        </p>
                    </div>
                    <%--  <div class="card-title">
                        <div class="row">
                            <h3>Associate Details</h3>
                            <span>
                                <input type="button" class=" pull-right mybutton btn btn-primary" onclick="showAssociateModal()" value="Add Associate" />
                            </span>
                        </div>
                    </div>--%>
                    <div class="row">
                        <div class="card-body">
                            <div class="table-responsive">
                                <asp:GridView ID="grdAssociate" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Associate Code">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnAssociateId" runat="server" Value='<%# Eval("ID") %>' />
                                                <asp:Label ID="lblAssociateCode" runat="server" Text='<%# Eval("AssociateCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Associate Name">
                                            <ItemTemplate>
                                                <asp:Label ID="txtAssociateName" runat="server" Text='<%# Eval("AssociateName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address_line1">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress1" runat="server" Text='<%# Eval("Address_line1") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address_line2">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress2" runat="server" Text='<%# Eval("Address_line2") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" runat="server" Text='<%# Eval("City") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State">
                                            <ItemTemplate>
                                                <asp:Label ID="lblState" runat="server" Text='<%# Eval("State") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Country">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("Country") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pincode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPincode" runat="server" Text='<%# Eval("Pincode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" OnClick="EditAssociate">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkCode" runat="server" OnClientClick="return DeleteAssociate()" OnClick="DeleteAssociate">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Records
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div class="modal fade" id="associateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Associate</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="ClosePopup();"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <%--<div class="text-center">
                                <asp:ValidationSummary ID="validationSummary" runat="server" ShowModelStateErrors="true" DisplayMode="List" ForeColor="Red" />
                            </div>--%>
                    <div class="row">
                        <div class="col-md-6">
                            <asp:HiddenField ID="hdnAsId" runat="server" />
                            <span class="form-control-sm" style="margin-left: -10px;">Code:<span class="text-danger"> *</span> </span>
                            <asp:TextBox ID="txtAssociateCode" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Name:<span class="text-danger"> *</span></span>
                            <asp:TextBox ID="txtAssociateName" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">City:<span class="text-danger"> *</span></span>
                            <asp:TextBox ID="txtCity" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">State:</span>
                            <asp:TextBox ID="txtState" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Country:</span>
                            <asp:TextBox ID="txtCountry" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Pincode:</span>
                            <asp:TextBox ID="txtPincode" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Address_line1:</span>
                            <asp:TextBox ID="txtAddress1" ClientIDMode="Static" CssClass="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Address_line2:</span>
                            <asp:TextBox ID="txtAddress2" ClientIDMode="Static" CssClass="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <span>
                        <asp:ValidationSummary ID="validationSummary" ClientIDMode="Static" runat="server" ShowModelStateErrors="true" DisplayMode="List" ForeColor="Red" />
                    </span>
                    <asp:Button ID="btnSaveAssociate" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveAssociate_Click" />
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="ClosePopup();">Close</button>
                </div>
            </div>
        </div>
    </div>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>


    <script type="text/javascript">


        function ShowPopup() {
            $("#associateModal").modal("show");
        }

        function ClosePopup() {
            debugger;
            $("#associateModal").modal("hide");
        }

        function DeleteAssociate() {
            if (confirm("Are you sure you want to delete this Associate?")) {
                return true;
            } else {
                return false;
            }
        }
    </script>
</asp:Content>
