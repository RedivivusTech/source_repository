﻿using HRMSTemplate.App_Code.BAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMSTemplate.Masters
{
    public partial class Client : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataFill();
            }
        }

        private void DataFill()
        {
            var data = clsClient.GetAll(Convert.ToInt32(Session["CompanyID"]));
            grdClient.DataSource = data;
            grdClient.DataBind();
        }

        protected void btnSaveClient_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                clsClient client = new clsClient();
                client.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                client.ClientID = (hdnClsId.Value == "") ? 0 : Convert.ToInt32(hdnClsId.Value);
                client.ClientCode = txtClientCode.Text;
                client.NoOfEmployee = Convert.ToInt32(txtNoOfEmp.Text);
                client.ParentClientID = (ddlParentClient.SelectedValue == "") ? 0 : Convert.ToInt32(ddlParentClient.SelectedValue);
                client.ClientName = txtClientName.Text;
                client.City = txtCity.Text;
                client.State = txtState.Text;
                client.Address_line1 = txtAddress1.Text;
                client.Address_line2 = txtAddress2.Text;
                client.BranchID = (ddlBranch.SelectedValue == "") ? 0 : Convert.ToInt32(ddlBranch.SelectedValue);
                client.RegistrationNo = txtRegNo.Text;
                client.Pincode = Convert.ToInt32(txtPincode.Text);
                client.Division = txtDivision.Text;
                client.PANNo = txtPANNo.Text;
                client.GSTNo = txtGSTNo.Text;
                client.Country = txtCountry.Text;
                client.ContactNo = txtContact.Text;
                client.EmailID = txtEmail.Text;
                int result = clsClient.SaveClient(client, Convert.ToInt32(Session["UID"]));
                if (result > 0)
                {
                    string msg = null;
                    if (client.ClientID == 0)
                    {
                        msg = "Successfully Inserted";
                    }
                    else
                    {
                        msg = "Successfully Updated";
                    }
                    //string title = "Done";
                    //ScriptManager.RegisterStartupScript(this, GetType(), "Success", "<script>showpop5('" + msg + "','" + title + "')</script>", true);
                    Response.Redirect("Client.aspx");
                }
                else
                    Response.Write("Not Inserted");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#clientModal').modal()", true);//show the modal
            }
        }


        protected void EditClient(object sender, EventArgs e)
        {
            clsClient objClient = new clsClient();
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            hdnClsId.Value = ((HiddenField)row.FindControl("hdnClientId")).Value;

            int clientId = Convert.ToInt32(hdnClsId.Value);
            objClient = clsClient.GetClientByID(clientId);

            //hdnBranchID.Value = branchId;
            txtClientCode.Text = objClient.ClientCode;
            txtClientName.Text = objClient.ClientName;
            txtAddress1.Text = objClient.Address_line1;
            txtAddress2.Text = objClient.Address_line2;
            txtCity.Text = objClient.City;
            txtState.Text = objClient.State;
            txtNoOfEmp.Text = objClient.NoOfEmployee.ToString();
            txtRegNo.Text = objClient.RegistrationNo;
            txtGSTNo.Text = objClient.GSTNo;
            txtPANNo.Text = objClient.PANNo;
            txtDivision.Text = objClient.Division;
            txtEmail.Text = objClient.EmailID;
            txtContact.Text = objClient.ContactNo.ToString();
            txtCountry.Text = objClient.Country;
            txtPincode.Text = objClient.Pincode.ToString();

            LoadParentClients();
            ddlParentClient.Items.FindByValue(objClient.ParentClientID.ToString()).Selected = true;
            LoadBranch();
            ddlBranch.Items.FindByValue(objClient.BranchID.ToString()).Selected = true;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

            //ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#clientModal').modal()", true);//show the modal
        }
        protected void DeleteClient(object sender, EventArgs e)
        {
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            HiddenField hdnClientID = (HiddenField)row.FindControl("hdnClientId");
            int clientID = Convert.ToInt32(hdnClientID.Value);  //stored Branch ID
            int result = clsClient.RemoveClient(clientID, Convert.ToInt32(Session["UID"]));
            if (result > 0)
            {

                Response.Redirect("Client.aspx");
            }
            else
            {
                Response.Write("Not Deleted");
            }

        }

        public void LoadParentClients()
        {
            DataSet dataSet = clsClient.GetClientNameAndID(Convert.ToInt32(Session["CompanyID"]));
            ddlParentClient.DataSource = dataSet;
            ddlParentClient.DataTextField = "ClientName";
            ddlParentClient.DataValueField = "ID";
            ddlParentClient.DataBind();
        }

        public void LoadBranch()
        {
            DataSet dataSet = clsBranch.GetBranchNameAndID(Convert.ToInt32(Session["CompanyID"]));
            ddlBranch.DataSource = dataSet;
            ddlBranch.DataTextField = "BranchName";
            ddlBranch.DataValueField = "ID";
            ddlBranch.DataBind();
        }
        protected void AddClient(object sender, EventArgs e)
        {
            ClearAllFields();
            LoadParentClients();
            LoadBranch();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

        }

        private void ClearAllFields()
        {
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtCity.Text = "";
            txtClientCode.Text = "";
            txtClientName.Text = "";
            txtNoOfEmp.Text = "";
            txtRegNo.Text = "";
            txtState.Text = "";
            txtPincode.Text = "";
            txtContact.Text = "";
            txtDivision.Text = "";
            txtEmail.Text = "";
            txtPANNo.Text = "";
            txtCountry.Text = "";
            txtGSTNo.Text = "";
        }

        protected void ddlParentClient_DataBound(object sender, EventArgs e)
        {
            ddlParentClient.Items.Insert(0, new ListItem("--Select Parent Client--", "0"));
        }

        protected void ddlBranch_DataBound(object sender, EventArgs e)
        {
            ddlBranch.Items.Insert(0, new ListItem("--Select Branch--", "0"));
        }
    }
}