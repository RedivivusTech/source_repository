﻿using HRMSTemplate.App_Code.BAL;
using HRMSTemplate.ValidationManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMSTemplate.Masters
{
    public partial class Branch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetBranch();
            }
        }

        private void GetBranch()
        {
            var data = clsBranch.GetAll(Convert.ToInt32(Session["CompanyID"]));
            grdBranch.DataSource = data;
            grdBranch.DataBind();
        }
        protected void btnSaveBranch_Click(object sender, EventArgs e)
        {
            clsBranch branch = new clsBranch();
            branch.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            branch.BranchID = (hdnBrId.Value == "") ? 0 : Convert.ToInt32(hdnBrId.Value);
            branch.BranchCode = txtBranchCode.Text;
            branch.BranchName = txtBranchName.Text;
            branch.City = txtCity.Text;
            branch.State = txtState.Text;
            branch.Address_line1 = txtAddress1.Text;
            branch.Address_line2 = txtAddress2.Text;
            branch.Country = txtCountry.Text;
            branch.Pincode = (txtPincode.Text == "") ? 0 : Convert.ToInt32(txtPincode.Text);
            string errorMsg = string.Empty;

            if (TryUpdateModel(branch, new FormValueProvider(ModelBindingExecutionContext)))
            {
                //Validate Name and Code at Insertion time
                if (branch.BranchID == 0)
                {
                    //Check Associate Name is Exist or Not
                    if (clsBranch.ValidateBranchIsExistorNot(txtBranchName.Text))
                    {
                        errorMsg = "Branch Name is Already Exist";
                    }
                    if (clsBranch.ValidateBranchCodeIsExistorNot(txtBranchCode.Text))
                    {
                        if (errorMsg == String.Empty)
                        {
                            errorMsg = "Branch Code is Already Exist";
                        }
                        else
                        {
                            errorMsg = errorMsg + "<br> Branch Code is Already Exist";
                        }
                    }
                }
                if (string.IsNullOrEmpty(errorMsg))
                {
                    int result = clsBranch.SaveBranch(branch, Convert.ToInt32(Session["UID"]));
                    if (result > 0)
                    {
                        string msg = null;
                        if (branch.BranchID == 0)
                        {
                            msg = "Successfully Inserted";
                        }
                        else
                        {
                            msg = "Successfully Updated";
                        }
                        string title = "Done";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Success", "<script>showpop5('" + msg + "','" + title + "')</script>", true);
                        GetBranch();
                    }
                    else
                        Response.Write("Not Inserted");
                }
                else
                {
                    //Added my custome error to validation summary
                    Page currentPage = HttpContext.Current.Handler as Page;
                    currentPage.Validators.Add(new ValidationError(errorMsg));
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
            }

        }

        protected void EditBranch(object sender, EventArgs e)
        {
            clsBranch objBranch = new clsBranch();
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            hdnBrId.Value = ((HiddenField)row.FindControl("hdnBranchId")).Value;

            int branchId = Convert.ToInt32(hdnBrId.Value);
            objBranch = clsBranch.GetBranchByID(branchId);

            //hdnBranchID.Value = branchId;
            txtBranchCode.Text = objBranch.BranchCode;
            txtBranchName.Text = objBranch.BranchName;
            txtAddress1.Text = objBranch.Address_line1;
            txtAddress2.Text = objBranch.Address_line2;
            txtCity.Text = objBranch.City;
            txtState.Text = objBranch.State;
            txtCountry.Text = objBranch.Country;
            txtPincode.Text = Convert.ToString(objBranch.Pincode);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

            //ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#myModal').modal()", true);//show the modal
        }
        protected void DeleteBranch(object sender, EventArgs e)
        {
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            HiddenField hdnBranchID = (HiddenField)row.FindControl("hdnBranchId");
            int branchID = Convert.ToInt32(hdnBranchID.Value);  //stored Branch ID
            int result = clsBranch.RemoveBranch(branchID, Convert.ToInt32(Session["UID"]));
            if (result > 0)
            {
                GetBranch();
                // Response.Redirect("Branch.aspx");
            }
            else
            {
                Response.Write("Not Deleted");
            }

        }
        private void ClearAllFields()
        {
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtCity.Text = "";
            txtBranchCode.Text = "";
            txtBranchName.Text = "";
            txtState.Text = "";
            txtCountry.Text = "";
            txtPincode.Text = "";
        }
        protected void btnAddBranch_Click(object sender, EventArgs e)
        {
            ClearAllFields();
            //ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#myModal').modal()", true);//show the modal
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

        }
    }
}