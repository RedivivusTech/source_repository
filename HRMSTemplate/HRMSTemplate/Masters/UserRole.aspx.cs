﻿using HRMSTemplate.App_Code.BAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMSTemplate.Masters
{
    public partial class UserRole : System.Web.UI.Page
    {
        public enum MessageType
        {
            Success,
            Error,
            Info,
            Warning
        };
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadParentRoles();
            }
        }

        private void LoadParentRoles()
        {
            DataSet ds = clsUserRole.GetParentRoleNameAndID();
            ddlParentRoles.DataSource = ds;
            ddlParentRoles.DataTextField = "role_name";
            ddlParentRoles.DataValueField = "User_RolesId";
            ddlParentRoles.DataBind();
        }

        protected void ddlParentRoles_DataBound(object sender, EventArgs e)
        {
            //ddlParentRoles.Items.Insert(0, new ListItem("--Select Parent Role--", "0"));
        }

        protected void ddlParentRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            int parentRoleID = (ddlParentRoles.SelectedValue == "0") ? 0 : Convert.ToInt32(ddlParentRoles.SelectedValue);
            DataTable dt = clsUserRole.GetAll(parentRoleID);
            rptUserRole.DataSource = dt;
            rptUserRole.DataBind();
        }

        private void LoadAllModules()
        {
            DataTable dt = clsUserRole.GetAllModules();
            rptModule.DataSource = dt;
            rptModule.DataBind();
        }

        protected void ddlParentRoles_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int parentRoleID = (ddlParentRoles.SelectedValue == "0") ? 0 : Convert.ToInt32(ddlParentRoles.SelectedValue);
                DataTable dt = clsUserRole.GetAll(parentRoleID);
                rptUserRole.DataSource = dt;
                rptUserRole.DataBind();
            }
        }
        protected void lnkView_Click(object sender, EventArgs e)
        {
            LinkButton linkbutton = (LinkButton)sender;
            var row = (RepeaterItem)linkbutton.NamingContainer;
            int userRoleId = Convert.ToInt32(((HiddenField)row.FindControl("hdnUserRoleID")).Value);

            DataTable dt = clsUserRoleModuleAccess.GetByUserRoleID(userRoleId, Convert.ToInt32(Session["CompanyID"]));
            rptModule.DataSource = dt;
            rptModule.DataBind();



        }

        protected void btnSaveModule_Click(object sender, EventArgs e)
        {
            foreach (RepeaterItem item in rptModule.Items)
            {
                CheckBox check = item.FindControl("chkModule") as CheckBox;
                HiddenField hdnModuleid = (HiddenField)item.FindControl("hdnModuleID");
                bool isActive;
                if (check.Checked)
                    isActive = true;
                else
                    isActive = false;

                clsUserRoleModuleAccess objModuleAccess = new clsUserRoleModuleAccess();
                objModuleAccess.UserRoleID = Convert.ToInt32(ViewState["UserRoleID"]);
                objModuleAccess.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objModuleAccess.ModuleID = Convert.ToInt32(hdnModuleid.Value);
                objModuleAccess.IsActive = isActive;
                int result = clsUserRoleModuleAccess.SaveUserRoleModuleAccess(objModuleAccess, Convert.ToInt32(Session["UID"]));
                if (isActive)
                {
                    var objUserPermission = new clsUserRolePermission();
                    objUserPermission.PermissionID = 1;  //Default permission Read
                    objUserPermission.UserRoleId = Convert.ToInt32(ViewState["UserRoleID"]);
                    objUserPermission.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                    objUserPermission.IsActive = true;  //Default permission Read IsActive
                    objUserPermission.Modules_ID = Convert.ToInt32(hdnModuleid.Value);
                    clsUserRolePermission.SaveUserRolePermission(objUserPermission, Convert.ToInt32(Session["UID"]));
                }
            }
        }

        protected void chkModule_CheckedChanged(object sender, EventArgs e)
        {
            //For getting ModuleID
            CheckBox checkBox = (CheckBox)sender;
            var row = (RepeaterItem)checkBox.NamingContainer;
            string moduleId = ((HiddenField)row.FindControl("hdnModuleID")).Value;

            int allCheckCount = 0;
            int checkedCount = 0;
            foreach (RepeaterItem item in rptModule.Items)
            {
                allCheckCount++;
                CheckBox check = item.FindControl("chkModule") as CheckBox;
                if (check.Checked)
                {
                    LinkButton lnkBtnPermission = (LinkButton)item.FindControl("lnkPermission");
                    lnkBtnPermission.Visible = true;
                    checkedCount++;
                }
                else
                {
                    LinkButton lnkBtnPermission = (LinkButton)item.FindControl("lnkPermission");
                    lnkBtnPermission.Visible = false;
                }

            }
            if (allCheckCount == checkedCount)  //If all module and total check box checked then Select All is true
            {
                Control HeaderTemplate = rptModule.Controls[0].Controls[0];
                CheckBox checkAllModule = HeaderTemplate.FindControl("chkAllModule") as CheckBox;
                checkAllModule.Checked = true;
            }
            else
            {
                Control HeaderTemplate = rptModule.Controls[0].Controls[0];
                CheckBox checkHeader = HeaderTemplate.FindControl("chkAllModule") as CheckBox;
                checkHeader.Checked = false;
            }
        }

        protected void rptUserRole_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            ViewState["UserRoleID"] = null;
            HiddenField userRoleID = e.Item.FindControl("hdnUserRoleID") as HiddenField; //getting hidden field value from Repeater One
            ViewState["UserRoleID"] = userRoleID.Value;
            lblUserRoleName.Text = e.CommandArgument.ToString();
            ViewState["UserRoleName"] = e.CommandArgument.ToString();  //This is used for permission popup to display
        }

        protected void chkAllModule_CheckedChanged(object sender, EventArgs e)
        {
            Control HeaderTemplate = rptModule.Controls[0].Controls[0];
            CheckBox checkAllModule = HeaderTemplate.FindControl("chkAllModule") as CheckBox;
            if (checkAllModule.Checked)
            {
                foreach (RepeaterItem item in rptModule.Items)
                {
                    LinkButton lnkBtnPermission = (LinkButton)item.FindControl("lnkPermission");
                    lnkBtnPermission.Visible = true;
                    CheckBox check = (CheckBox)item.FindControl("chkModule");
                    check.Checked = true;
                }
            }
            else
            {
                foreach (RepeaterItem item in rptModule.Items)
                {
                    LinkButton lnkBtnPermission = (LinkButton)item.FindControl("lnkPermission");
                    lnkBtnPermission.Visible = false;
                    CheckBox check = (CheckBox)item.FindControl("chkModule");
                    check.Checked = false;
                }
            }
        }

        protected void lnkPermission_Click(object sender, EventArgs e)
        {
            //LoadAllPermission();
            LinkButton link = (LinkButton)sender;
            var row = (RepeaterItem)link.NamingContainer;
            string moduleId = ((HiddenField)row.FindControl("hdnModuleID")).Value;

            ViewState["ModuleIDForPermission"] = null;
            ViewState["ModuleIDForPermission"] = moduleId;

            var objUserPermission = new clsUserRolePermission();
            objUserPermission.UserRoleId = Convert.ToInt32(ViewState["UserRoleID"]);
            objUserPermission.Modules_ID = Convert.ToInt32(ViewState["ModuleIDForPermission"]);
            objUserPermission.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            DataTable dt = clsUserRolePermission.GetByUserPermissionByID(objUserPermission);
            rptUserRolePermission.DataSource = dt;
            rptUserRolePermission.DataBind();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

            //ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#userRolePermissionModal').modal()", true);//show the modal

        }

        //private void LoadAllPermission()
        //{
        //    DataSet ds = clsUserRolePermission.GetPermissions();
        //    rptUserRolePermission.DataSource = ds;
        //    rptUserRolePermission.DataBind();
        //}

        protected void chkPermissionHerder_CheckedChanged(object sender, EventArgs e)
        {
            Control HeaderTemplate = rptUserRolePermission.Controls[0].Controls[0];
            CheckBox checkAllPermission = HeaderTemplate.FindControl("chkPermissionHerder") as CheckBox;
            if (checkAllPermission.Checked)
            {
                foreach (RepeaterItem item in rptUserRolePermission.Items)
                {
                    CheckBox check = (CheckBox)item.FindControl("chkPermission");
                    check.Checked = true;
                }
            }
            else
            {
                foreach (RepeaterItem item in rptUserRolePermission.Items)
                {
                    CheckBox check = (CheckBox)item.FindControl("chkPermission");
                    check.Checked = false;
                }
            }
        }

        protected void chkPermission_CheckedChanged(object sender, EventArgs e)
        {
            int allCheckCount = 0;
            int checkedCount = 0;
            foreach (RepeaterItem item in rptUserRolePermission.Items)
            {
                allCheckCount++;
                CheckBox check = item.FindControl("chkPermission") as CheckBox;
                if (check.Checked)
                {
                    checkedCount++;
                }
            }
            if (allCheckCount == checkedCount)  //If all module and total check box checked then Select All is true
            {
                Control HeaderTemplate = rptUserRolePermission.Controls[0].Controls[0];
                CheckBox checkAllModule = HeaderTemplate.FindControl("chkPermissionHerder") as CheckBox;
                checkAllModule.Checked = true;
            }
            else
            {
                Control HeaderTemplate = rptUserRolePermission.Controls[0].Controls[0];
                CheckBox checkHeader = HeaderTemplate.FindControl("chkPermissionHerder") as CheckBox;
                checkHeader.Checked = false;
            }
        }

        protected void rptModule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                CheckBox cbox = e.Item.FindControl("chkModule") as CheckBox;
                if (cbox != null)
                {
                    cbox.Checked = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "is_active"));
                    LinkButton lnkBtnPermission = (LinkButton)e.Item.FindControl("lnkPermission");
                    lnkBtnPermission.Visible = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "is_active"));
                }
            }
        }

        protected void rptUserRolePermission_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                CheckBox cbox = e.Item.FindControl("chkPermission") as CheckBox;
                if (cbox != null)
                {
                    cbox.Checked = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "is_active"));
                }
            }
        }

        protected void btnSavePermission_Click(object sender, EventArgs e)
        {
            string msg = string.Empty;
            int result = 0;
            foreach (RepeaterItem item in rptUserRolePermission.Items)
            {
                CheckBox check = item.FindControl("chkPermission") as CheckBox;
                HiddenField hdnPermissionid = (HiddenField)item.FindControl("hdnPermissionID");
                bool isActive;
                if (check.Checked)
                    isActive = true;
                else
                    isActive = false;

                clsUserRolePermission objPermission = new clsUserRolePermission();
                objPermission.UserRoleId = Convert.ToInt32(ViewState["UserRoleID"]);
                objPermission.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                objPermission.PermissionID = Convert.ToInt32(hdnPermissionid.Value);
                objPermission.IsActive = isActive;
                objPermission.Modules_ID = Convert.ToInt32(ViewState["ModuleIDForPermission"]);

                //Before inserting permissions check if Module Access or not
                int value = clsUserRoleModuleAccess.CheckModuleAccessForPermission(objPermission.UserRoleId, objPermission.Modules_ID);
                if (value > 0)
                    result = clsUserRolePermission.SaveUserRolePermission(objPermission, Convert.ToInt32(Session["UID"]));
                else
                {
                    msg = "Please First Update Module Access then assign Permissions";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "ShowMessage('" + msg + "','" + MessageType.Warning + "');", true);
                }
            }
            if (result > 0)
            {
                msg = "Permissions Updated Successfully";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "ShowMessage('" + msg + "','" + MessageType.Success + "');", true);
            }
            else
            {
                msg = "Permissions Not Updated";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "ShowMessage('" + msg + "','" + MessageType.Error + "');", true);
            }

        }

        protected void rptModule_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            lblModuleName.Text = e.CommandArgument.ToString();
            lblUserRole.Text = Convert.ToString(ViewState["UserRoleName"]);
        }
    }
}