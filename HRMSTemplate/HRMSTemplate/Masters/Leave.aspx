﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Leave.aspx.cs" Inherits="HRMSTemplate.Masters.Leave" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div>
                        <p class="card-title">
                            Leave Details
                            <span style="float: right">
                                <asp:Button ID="btnAddLeave" runat="server" Text="Add Leave" CssClass="pull-right mybutton btn btn-primary" CausesValidation="false" OnClick="AddLeave" />
                            </span>
                        </p>
                    </div>

                    <%--<div class="row">
                        <h3>Leave Details</h3>
                        <span>
                            <asp:Button ID="btnAddLeave" runat="server" Text="Add Leave" CssClass="pull-right mybutton btn btn-primary" CausesValidation="false" OnClick="AddLeave" />
                        </span>
                    </div>--%>

                    <div class="row">
                        <div class="card-body">
                            <div class="table-responsive">
                                <asp:GridView ID="grdLeave" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Leave Name">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnLeaveId" runat="server" Value='<%# Eval("LeaveMID") %>' />
                                                <asp:Label ID="lblLeaveName" runat="server" Text='<%# Eval("LeaveName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Short Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblShortName" runat="server" Text='<%# Eval("ShortName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No of Leaves">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNoOfLeaves" runat="server" Text='<%# Eval("NoOfLeaves") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Payment Ratio">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentRatio" runat="server" Text='<%# Eval("PaymentRatio") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Client Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClientName" runat="server" Text='<%# Eval("ClientName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" OnClick="EditLeave">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkCode" runat="server" OnClientClick="return DeleteLeave()" OnClick="DeleteLeave">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Records

                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="leaveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Leave</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <asp:HiddenField ID="hdnLevId" runat="server" />
                            <span class="form-control-sm" style="margin-left: -10px;">Leave Name:</span>
                            <asp:TextBox ID="txtLeaveName" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Short Name:</span>
                            <asp:TextBox ID="txtShortName" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Leave Year:</span>
                            <asp:DropDownList ID="ddlYear" ClientIDMode="Static" DataTextField="FinancialYear" DataValueField="ID" runat="server" OnDataBound="ddlYear_DataBound" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Client:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:DropDownList ID="ddlClient" ClientIDMode="Static" DataTextField="ClientName" DataValueField="ID" runat="server" OnDataBound="ddlClient_DataBound" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Payment Ratio:</span>
                            <asp:TextBox ID="txtPaymentRatio" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">No Of Leaves:</span>
                            <asp:TextBox ID="txtNoOfLeaves" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnSaveLeave" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveLeave_Click" />
                    <button id="btnClose" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        function DeleteLeave() {
            if (confirm("Are you sure you want to delete this Leave?")) {
                return true;
            } else {
                return false;
            }
        }

        function showLeaveModal() {
            $('#txtLeaveName').val("");
            $('#txtShortName').val("");
            $('#txtPaymentRatio').val("");
            $('#txtNoOfLeaves').val("");
            $('#leaveModal').modal('show');
        }

        function ShowPopup() {
            $("#leaveModal").modal("show");
        }

        $(function () {
            $("#btnClose").click(function () {
                $("#leaveModal").modal('hide');
            });
        });

        $(document).ready(function () {
            $("#<%=ddlYear.ClientID%>").select2({
                //placeholder: "Select Item",
                allowClear: true
            });

            $("#<%=ddlClient.ClientID%>").select2({
                //placeholder: "Select Item",
                allowClear: true
            });
        });
    </script>
</asp:Content>
