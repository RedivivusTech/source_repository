﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserRole.aspx.cs" Inherits="HRMSTemplate.Masters.UserRole" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <div class="row">
        <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div>
                        <p class="card-title">
                            User Role Details
                        </p>
                    </div>

                    <%--<div class="panel-heading">
                        <div class="row">
                            <div class="col-md-9">
                                <h3>User Role Details</h3>
                            </div>
                            <%--<div class="col-md-3" style="margin-top: 25px;">
                        <span class="pull-right">
                            <asp:DropDownList ID="ddlParentRoles" CssClass="form-control" runat="server" AutoPostBack="true" DataTextField="role_name" DataValueField="User_RolesId" OnDataBound="ddlParentRoles_DataBound" OnSelectedIndexChanged="ddlParentRoles_SelectedIndexChanged"></asp:DropDownList>
                        </span>
                    </div>
                        </div>
                    </div>--%>
                    <div class="row">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <asp:DropDownList ID="ddlParentRoles" CssClass="form-control" runat="server" AutoPostBack="true" DataTextField="role_name" DataValueField="User_RolesId" OnDataBound="ddlParentRoles_DataBound" OnSelectedIndexChanged="ddlParentRoles_SelectedIndexChanged" OnLoad="ddlParentRoles_Load"></asp:DropDownList>
                                    <br />
                                    <hr />
                                    <asp:Repeater ID="rptUserRole" runat="server" OnItemCommand="rptUserRole_ItemCommand">
                                        <HeaderTemplate>
                                            <table class="table-bordered table-condensed table-responsive">
                                                <tr>
                                                    <th>Roles</th>
                                                    <th>Module Access</th>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="hdnUserRoleID" runat="server" Value='<%# Eval("User_RolesId") %>' />
                                                    <%#Eval("role_name") %>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkView" runat="server" OnClick="lnkView_Click" CommandArgument='<%# Eval("role_name") %>'>View</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="col-md-8 pull-right">
                                    <div>
                                        <h4>
                                            <asp:Label ID="lblUserRoleName" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnSaveModule" runat="server" CssClass="btn btn-primary" Text="Update" OnClick="btnSaveModule_Click" />
                                        </h4>
                                    </div>
                                    <asp:Repeater ID="rptModule" runat="server" OnItemDataBound="rptModule_ItemDataBound" OnItemCommand="rptModule_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="tblModule" class="table-bordered table-condensed table-responsive">
                                                <tr>
                                                    <th>
                                                        <div class="form-check">
                                                            <label class="form-check-label">

                                                                <asp:CheckBox ID="chkAllModule" ClientIDMode="Static" AutoPostBack="true" runat="server" OnCheckedChanged="chkAllModule_CheckedChanged" />

                                                                <%--<input type="checkbox" class="form-check-input">--%>
                                                            </label>
                                                        </div>

                                                    </th>
                                                    <th>Modules</th>
                                                    <th>Permission Access</th>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <asp:CheckBox ID="chkModule" runat="server" AutoPostBack="true" OnCheckedChanged="chkModule_CheckedChanged" />
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hdnURoleID" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdnModuleID" runat="server" Value='<%# Eval("Modules_ID") %>' />
                                                    <%--<asp:HiddenField ID="hdnIsActive" runat="server" />--%>

                                                    <%#Eval("ModulesName") %>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkPermission" runat="server" Visible="false" ClientIDMode="Static" CommandArgument=' <%#Eval("ModulesName") %>' OnClick="lnkPermission_Click">View</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="userRolePermissionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Module Permission</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row" style="margin-left: 0px;">
                                <div class="col-md-6">
                                    <h5>Module Name: <span class="label label-primary">
                                        <asp:Label ID="lblModuleName" runat="server" Text="Label"></asp:Label></span></h5>
                                    <h5>User Role Name: <span class="label label-primary">
                                        <asp:Label ID="lblUserRole" runat="server" Text="Label"></asp:Label></span></h5>
                                </div>
                                <div class="col-md-6">
                                    <asp:Repeater ID="rptUserRolePermission" runat="server" OnItemDataBound="rptUserRolePermission_ItemDataBound">
                                        <HeaderTemplate>
                                            <table class="table-bordered table-condensed table-responsive">
                                                <tr>
                                                    <th>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                <asp:CheckBox ID="chkPermissionHerder" runat="server" AutoPostBack="true" OnCheckedChanged="chkPermissionHerder_CheckedChanged" />
                                                            </label>
                                                        </div>
                                                    </th>
                                                    <th>Permissions</th>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <asp:CheckBox ID="chkPermission" runat="server" AutoPostBack="true" OnCheckedChanged="chkPermission_CheckedChanged" />
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <%-- <asp:HiddenField ID="hdnUserRoleIDForPermission" runat="server" />
                                                        <asp:HiddenField ID="hdnModuleIDForPermission" runat="server" />--%>
                                                    <asp:HiddenField ID="hdnPermissionID" runat="server" Value='<%# Eval("PermissionID") %>' />
                                                    <%#Eval("Permission_Name") %>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnSavePermission" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="btnSavePermission_Click" />
                    <button id="btnClose" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=ddlParentRoles.ClientID%>").select2({
                //placeholder: "Select Item",
                allowClear: true
            });

            $("input[type=checkbox]").addClass("form-check-input");
        });

        function ShowPopup() {
            $("#userRolePermissionModal").modal("show");
        }
        //function ShowMessage(message, messagetype) {
        //    var cssclass;
        //    switch (messagetype) {
        //        case 'Success':
        //            cssclass = 'alert-success'
        //            break;
        //        case 'Error':
        //            cssclass = 'alert-danger'
        //            break;
        //        case 'Warning':
        //            cssclass = 'alert-warning'
        //            break;
        //        default:
        //            cssclass = 'alert-info'
        //    }
        //    $('#alert_container').append('<div id="alert_div" style="margin: 0 0.5%; -webkit-box-shadow: 3px 4px 6px #999;" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + messagetype + '!</strong> <span>' + message + '</span></div>');
        //    $('#alert_container').delay(3000).fadeOut();
        //}
    </script>

</asp:Content>
