﻿using HRMSTemplate.App_Code.BAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMSTemplate.Masters
{
    public partial class User : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataFill();
            }
        }

        private void DataFill()
        {
            var data = clsUser.GetAll(Convert.ToInt32(Session["CompanyID"]));
            grdUser.DataSource = data;
            grdUser.DataBind();
        }

        protected void AddUser(object sender, EventArgs e)
        {

            LoadParentUserRoles();
            LoadClients();
            LoadAssociate();
            ClearAllFields();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

            //ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#userModal').modal()", true);//show the modal

        }

        private void LoadAssociate()
        {
            DataSet dataSet = clsAssociate.GetAssociateNameAndID(Convert.ToInt32(Session["CompanyID"]));
            ddlAssociate.DataSource = dataSet;
            ddlAssociate.DataTextField = "AssociateName";
            ddlAssociate.DataValueField = "ID";
            ddlAssociate.DataBind();
        }

        private void LoadClients()
        {
            DataSet dataSet = clsClient.GetClientNameAndID(Convert.ToInt32(Session["CompanyID"]));
            ddlClient.DataSource = dataSet;
            ddlClient.DataTextField = "ClientName";
            ddlClient.DataValueField = "ID";
            ddlClient.DataBind();
        }

        private void LoadParentUserRoles()
        {
            DataSet ds = clsUserRole.GetParentRoleNameAndID();
            ddlParentUserRole.DataSource = ds;
            ddlParentUserRole.DataTextField = "role_name";
            ddlParentUserRole.DataValueField = "User_RolesId";
            ddlParentUserRole.DataBind();
        }

        private void LoadUserRole()
        {
            int parentRoleID = (ddlParentUserRole.SelectedValue == "0") ? 0 : Convert.ToInt32(ddlParentUserRole.SelectedValue);
            DataTable dt = clsUserRole.GetAll(parentRoleID);
            ddlUserRole.DataSource = dt;
            ddlUserRole.DataTextField = "role_name";
            ddlUserRole.DataValueField = "User_RolesId";
            ddlUserRole.DataBind();
        }

        private void ClearAllFields()
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
        }
        protected void ddlParentUser_DataBound(object sender, EventArgs e)
        {
            ddlParentUserRole.Items.Insert(0, new ListItem("--Select Parent User--", "0"));
        }

        protected void ddlUserRole_DataBound(object sender, EventArgs e)
        {
            ddlUserRole.Items.Insert(0, new ListItem("--Select User Role--", "0"));
        }
        protected void ddlParentUserRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            int parentRoleID = (ddlParentUserRole.SelectedValue == "0") ? 0 : Convert.ToInt32(ddlParentUserRole.SelectedValue);
            DataTable dt = clsUserRole.GetAll(parentRoleID);
            ddlUserRole.DataSource = dt;
            ddlUserRole.DataTextField = "role_name";
            ddlUserRole.DataValueField = "User_RolesId";
            ddlUserRole.DataBind();
            //ClientScript.RegisterStartupScript(GetType(), "displayalertmessage", "$('#userModal').modal()", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "ApplyCss()", true);//show the modal

        }

        protected void ddlClient_DataBound(object sender, EventArgs e)
        {
            ddlClient.Items.Insert(0, new ListItem("--Select Client--", "0"));
        }

        protected void ddlAssociate_DataBound(object sender, EventArgs e)
        {
            ddlAssociate.Items.Insert(0, new ListItem("--Select Associate--", "0"));
        }
        protected void btnSaveUser_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                clsUser user = new clsUser();
                user.CompanyID = Convert.ToInt32(Session["CompanyID"]);
                user.UserID = (hdnUsrId.Value == "") ? 0 : Convert.ToInt32(hdnUsrId.Value);
                user.ClientID = (ddlClient.SelectedValue == "") ? 0 : Convert.ToInt32(ddlClient.SelectedValue);
                user.LoginName = txtUserName.Text;
                user.Password = txtPassword.Text;
                user.ParentRoleID = (ddlParentUserRole.SelectedValue == "") ? 0 : Convert.ToInt32(ddlParentUserRole.SelectedValue);
                user.UserRoleID = (ddlUserRole.SelectedValue == "") ? 0 : Convert.ToInt32(ddlUserRole.SelectedValue);
                user.AssociateID = (ddlAssociate.SelectedValue == "") ? 0 : Convert.ToInt32(ddlAssociate.SelectedValue);

                //Check if Already User Eixst or Not
                if (user.UserID == 0)
                {
                    bool isExist = clsUser.ValidateUserIsExistorNot(txtUserName.Text);
                    if (isExist)
                        ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "alert('This User is Already Exist')", true);//show the modal
                }
                else
                {
                    int result = clsUser.SaveUser(user, Convert.ToInt32(Session["UID"]));
                    if (result > 0)
                    {
                        DataFill();
                    }
                    else
                        Response.Write("Not Inserted");
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
                //ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#userModal').modal()", true);//show the modal
            }
        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            clsUser objUser = new clsUser();
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            hdnUsrId.Value = ((HiddenField)row.FindControl("hdnUserId")).Value;

            int userId = Convert.ToInt32(hdnUsrId.Value);
            objUser = clsUser.GetUserByID(userId);

            //hdnBranchID.Value = branchId;
            txtUserName.Text = objUser.LoginName;
            txtPassword.Text = objUser.Password;

            LoadParentUserRoles();
            ddlParentUserRole.Items.FindByValue(objUser.ParentRoleID.ToString()).Selected = true;
            LoadUserRole();
            ddlUserRole.Items.FindByValue(objUser.UserRoleID.ToString()).Selected = true;
            LoadClients();
            ddlClient.Items.FindByValue(objUser.ClientID.ToString()).Selected = true;
            LoadAssociate();
            ddlAssociate.Items.FindByValue(objUser.AssociateID.ToString()).Selected = true;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

        }

        protected void lnkModuleView_Click(object sender, EventArgs e)
        {
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            Session["UserID"] = null;
            Session["UserID"] = ((HiddenField)row.FindControl("hdnUserId")).Value;
            Session["UserRoleID"] = null;
            Session["UserRoleID"] = ((HiddenField)row.FindControl("hdnUserRoleID")).Value;
            //store UserName for Display in User Module Web Form
            Session["UserName"] = Convert.ToString(((sender as LinkButton).CommandArgument));
            Response.Redirect("~/Masters/UserModule.aspx");
        }

        protected void ddlEmployee_DataBound(object sender, EventArgs e)
        {
            ddlEmployee.Items.Insert(0, new ListItem("--Select Employee--", "0"));
        }
    }
}