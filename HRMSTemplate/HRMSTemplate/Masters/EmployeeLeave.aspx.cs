﻿using HRMSTemplate.App_Code.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMSTemplate.Masters
{
    public partial class EmployeeLeave : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                DataFill();
        }
        private void DataFill()
        {
            //var data = clsEmployeeLeave.GetAll(Convert.ToInt32(Session["CompanyID"]));
            //grdLeave.DataSource = data;
            //grdLeave.DataBind();
        }

        protected void AddEmployeeLeave(object sender, EventArgs e)
        {
            ClearAllFields();
            LoadLeaveTypes();
            LoadLeaveSubType();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
        }

        private void LoadLeaveSubType()
        {
            ddlLeaveSubType.Items.Insert(0, new ListItem("--Select Leave--", "0"));
            ddlLeaveSubType.Items.Insert(1, new ListItem("Half Day", "1"));
            ddlLeaveSubType.Items.Insert(2, new ListItem("Full Day", "2"));
        }
        private void ClearAllFields()
        {
            txtFromDate.Text = "";
            txtToDate.Text = "";
            txtLeaveAddress.Text = "";
            txtLeaveReason.Text = "";
            txtNoofLeave.Text = "";
        }

        private void LoadLeaveTypes()
        {
            var data = clsLeave.GetLeaveNameAndID(Convert.ToInt32(Session["CompanyID"]));
            ddlLeaveType.DataSource = data;
            ddlLeaveType.DataTextField = "LeaveName";
            ddlLeaveType.DataValueField = "LeaveMID";
            ddlLeaveType.DataBind();
        }

        protected void ddlLeaveType_DataBound(object sender, EventArgs e)
        {
            ddlLeaveType.Items.Insert(0, new ListItem("--Select Leave--", "0"));
        }

        protected void btnSaveEmployeeLeave_Click(object sender, EventArgs e)
        {
            clsEmployeeLeave objLeave = new clsEmployeeLeave();
            objLeave.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objLeave.ID = (hdnLeaveReqId.Value == "") ? 0 : Convert.ToInt32(hdnLeaveReqId.Value);
            objLeave.NoOfLeave = Convert.ToInt32(txtNoofLeave.Text);
            objLeave.LeaveID = (ddlLeaveType.SelectedValue == "") ? 0 : Convert.ToInt32(ddlLeaveType.SelectedValue);
            objLeave.EmployeeID = Convert.ToInt32(Session["EmployeeID"]);
            objLeave.LeaveReason = txtLeaveReason.Text;
            objLeave.LeaveAddress = txtLeaveAddress.Text;
            objLeave.FromDate = Convert.ToDateTime(txtFromDate.Text);
            objLeave.ToDate = Convert.ToDateTime(txtToDate.Text);
            objLeave.LeaveSubType = (ddlLeaveSubType.SelectedValue == "") ? 0 : Convert.ToInt32(ddlLeaveSubType.SelectedValue);

            int result = clsEmployeeLeave.SaveEmployeeLeaves(objLeave, Convert.ToInt32(Session["UID"]));
            if (result > 0)
            {
                string msg = null;
                if (objLeave.ID == 0)
                {
                    msg = "Successfully Inserted";
                }
                else
                {
                    msg = "Successfully Updated";
                }
                DataFill();
            }
            else
                Response.Write("Not Inserted");

        }


    }
}