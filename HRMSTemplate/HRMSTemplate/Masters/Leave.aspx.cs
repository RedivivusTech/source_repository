﻿using HRMSTemplate.App_Code.BAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMSTemplate.Masters
{
    public partial class Leave : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                DataFill();
        }

        private void DataFill()
        {
            var data = clsLeave.GetAll(Convert.ToInt32(Session["CompanyID"]));
            grdLeave.DataSource = data;
            grdLeave.DataBind();
        }

        protected void AddLeave(object sender, EventArgs e)
        {
            LoadClients();
            LoadYears();
            ClearAllFields();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

            //ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#leaveModal').modal()", true);//show the modal
        }

        private void ClearAllFields()
        {
            txtLeaveName.Text = "";
            txtShortName.Text = "";
            txtNoOfLeaves.Text = "";
            txtPaymentRatio.Text = "";
        }

        private void LoadYears()
        {
            DataSet dataSet = clsFinancialYear.GetYearNameAndID(Convert.ToInt32(Session["CompanyID"]));
            ddlYear.DataSource = dataSet;
            ddlYear.DataTextField = "FinancialYear";
            ddlYear.DataValueField = "ID";
            ddlYear.DataBind();
        }

        private void LoadClients()
        {
            DataSet dataSet = clsClient.GetClientNameAndID(Convert.ToInt32(Session["CompanyID"]));
            ddlClient.DataSource = dataSet;
            ddlClient.DataTextField = "ClientName";
            ddlClient.DataValueField = "ID";
            ddlClient.DataBind();
        }
        protected void btnSaveLeave_Click(object sender, EventArgs e)
        {
            clsLeave objLeave = new clsLeave();
            objLeave.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objLeave.LeaveID = (hdnLevId.Value == "") ? 0 : Convert.ToInt32(hdnLevId.Value);
            objLeave.NoOfLeaves = Convert.ToInt32(txtNoOfLeaves.Text);
            objLeave.ClientID = (ddlClient.SelectedValue == "") ? 0 : Convert.ToInt32(ddlClient.SelectedValue);
            objLeave.LeaveName = txtLeaveName.Text;
            objLeave.ShortName = txtShortName.Text;
            objLeave.PaymentRatio = txtPaymentRatio.Text;
            objLeave.LeaveYearId = (ddlYear.SelectedValue == "") ? 0 : Convert.ToInt32(ddlYear.SelectedValue);
            int result = clsLeave.SaveLeaves(objLeave, Convert.ToInt32(Session["UID"]));
            if (result > 0)
            {
                string msg = null;
                if (objLeave.LeaveID == 0)
                {
                    msg = "Successfully Inserted";
                }
                else
                {
                    msg = "Successfully Updated";
                }
                Response.Redirect("Leave.aspx");
            }
            else
                Response.Write("Not Inserted");

        }
        protected void EditLeave(object sender, EventArgs e)
        {
            clsLeave objLeave = new clsLeave();
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            hdnLevId.Value = ((HiddenField)row.FindControl("hdnLeaveId")).Value;

            int leaveId = Convert.ToInt32(hdnLevId.Value);
            objLeave = clsLeave.GetLeavesByID(leaveId);

            //hdnBranchID.Value = branchId;
            txtLeaveName.Text = objLeave.LeaveName;
            txtShortName.Text = objLeave.ShortName;
            txtNoOfLeaves.Text = objLeave.NoOfLeaves.ToString();
            txtPaymentRatio.Text = objLeave.PaymentRatio;
            LoadClients();
            ddlClient.Items.FindByValue(objLeave.ClientID.ToString()).Selected = true;
            LoadYears();
            ddlYear.Items.FindByValue(objLeave.LeaveYearId.ToString()).Selected = true;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

            //ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#leaveModal').modal()", true);//show the modal
        }
        protected void DeleteLeave(object sender, EventArgs e)
        {
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            HiddenField hdnID = (HiddenField)row.FindControl("hdnLeaveId");
            int leaveID = Convert.ToInt32(hdnID.Value);  //stored Leave ID
            int result = clsLeave.RemoveLeaves(leaveID, Convert.ToInt32(Session["UID"]));
            if (result > 0)
            {
                Response.Redirect("Leave.aspx");
            }
            else
            {
                Response.Write("Not Deleted");
            }
        }
        protected void ddlClient_DataBound(object sender, EventArgs e)
        {
            ddlClient.Items.Insert(0, new ListItem("--Select Client--", "0"));
        }

        protected void ddlYear_DataBound(object sender, EventArgs e)
        {
            ddlYear.Items.Insert(0, new ListItem("--Select Year--", "0"));
        }
    }
}