﻿using HRMSTemplate.App_Code.BAL;
using HRMSTemplate.ValidationManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMSTemplate.Masters
{
    public partial class Department : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataFill();
            }
        }

        private void DataFill()
        {
            var data = clsDepartment.GetAll(Convert.ToInt32(Session["CompanyID"]));
            grdDepartment.DataSource = data;
            grdDepartment.DataBind();
        }

        protected void btnSaveDepartment_Click(object sender, EventArgs e)
        {
            clsDepartment dept = new clsDepartment();
            dept.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            dept.DepartmentID = (hdnDeptsId.Value == "") ? 0 : Convert.ToInt32(hdnDeptsId.Value);
            dept.DepartmentCode = txtDepartmentCode.Text;
            dept.ParentDeptID = (ddlParentDepartment.SelectedValue == "") ? 0 : Convert.ToInt32(ddlParentDepartment.SelectedValue);
            dept.DepartmentName = txtDepartmentName.Text;
            string errorMsg = string.Empty;
            if (TryUpdateModel(dept, new FormValueProvider(ModelBindingExecutionContext)))
            {
                //Validate Name and Code at Insertion time 
                if (dept.DepartmentID == 0)
                {
                    //Check Associate Name is Exist or Not
                    if (clsDepartment.ValidateDepartmentIsExistorNot(txtDepartmentName.Text))
                    {
                        errorMsg = "Department Name is Already Exist";
                    }
                    if (clsDepartment.ValidateDepartmenCodetIsExistorNot(txtDepartmentCode.Text))
                    {
                        if (errorMsg == String.Empty)
                        {
                            errorMsg = "Department Code is Already Exist";
                        }
                        else
                        {
                            errorMsg = errorMsg + "<br> Department Code is Already Exist";
                        }
                    }
                    if (dept.ParentDeptID == 0)
                        errorMsg = errorMsg + "<br> Please Select Parent Department";
                }
                if (string.IsNullOrEmpty(errorMsg))
                {
                    int result = clsDepartment.SaveDepartment(dept, Convert.ToInt32(Session["UID"]));
                    if (result > 0)
                    {
                        string msg = null;
                        if (dept.DepartmentID == 0)
                        {
                            msg = "Successfully Inserted";
                        }
                        else
                        {
                            msg = "Successfully Updated";
                        }
                        Response.Redirect("Department.aspx");
                    }
                    else
                        Response.Write("Not Inserted");
                }
                else
                {
                    //Added my custome error to validation summary
                    Page currentPage = HttpContext.Current.Handler as Page;
                    currentPage.Validators.Add(new ValidationError(errorMsg));
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
                }
            }
            else
            {
                if (dept.ParentDeptID == 0)
                    errorMsg = errorMsg + "Please Select Parent Department";

                Page currentPage = HttpContext.Current.Handler as Page;
                currentPage.Validators.Add(new ValidationError(errorMsg));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
            }
        }
        protected void EditDepartment(object sender, EventArgs e)
        {
            clsDepartment objDept = new clsDepartment();
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            hdnDeptsId.Value = ((HiddenField)row.FindControl("hdnDepartmentId")).Value;

            int deptId = Convert.ToInt32(hdnDeptsId.Value);
            objDept = clsDepartment.GetDepartmentByID(deptId);

            txtDepartmentCode.Text = objDept.DepartmentCode;
            txtDepartmentName.Text = objDept.DepartmentName;

            LoadParentDepartments();
            ddlParentDepartment.Items.FindByValue(objDept.ParentDeptID.ToString()).Selected = true;

            ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#deptModal').modal()", true);//show the modal
        }

        protected void DeleteDepartment(object sender, EventArgs e)
        {
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            HiddenField hdndeptID = (HiddenField)row.FindControl("hdnDepartmentId");
            int deptID = Convert.ToInt32(hdndeptID.Value);  //stored Branch ID
            int result = clsDepartment.RemoveDepartment(deptID, Convert.ToInt32(Session["UID"]));
            if (result > 0)
            {
                Response.Redirect("Department.aspx");
            }
            else
            {
                Response.Write("Not Deleted");
            }
        }

        protected void AddDepartment(object sender, EventArgs e)
        {
            LoadParentDepartments();
            ClearAllFields();
            //ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#deptModal').modal()", true);//show the modal
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { showDepartmentModal(); });", true);

        }

        private void ClearAllFields()
        {
            txtDepartmentCode.Text = "";
            txtDepartmentName.Text = "";
        }

        private void LoadParentDepartments()
        {
            DataSet dataSet = clsDepartment.GetDepartmentNameAndID(Convert.ToInt32(Session["CompanyID"]));
            ddlParentDepartment.DataSource = dataSet;
            ddlParentDepartment.DataTextField = "DepartmentName";
            ddlParentDepartment.DataValueField = "ID";
            ddlParentDepartment.DataBind();
        }
        protected void ddlParentDepartment_DataBound(object sender, EventArgs e)
        {
            ddlParentDepartment.Items.Insert(0, new ListItem("--Select Parent Department--", "0"));
        }
    }
}