﻿using HRMSTemplate.App_Code.BAL;
using HRMSTemplate.ValidationManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMSTemplate.Masters
{
    public partial class Holiday : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetHolidays();
            }
        }
        private void GetHolidays()
        {
            var data = clsHoliday.GetAll(Convert.ToInt32(Session["CompanyID"]));
            grdHoliday.DataSource = data;
            grdHoliday.DataBind();
        }

        protected void btnSaveHoliday_Click(object sender, EventArgs e)
        {
            clsHoliday objHoliday = new clsHoliday();
            objHoliday.ID = (hdnHodId.Value == "") ? 0 : Convert.ToInt32(hdnHodId.Value);
            objHoliday.FinancialYear = (txtYear.Text == "") ? 0 : Convert.ToInt32(txtYear.Text);
            objHoliday.HolidayName = txtHolidayName.Text;
            objHoliday.HolidayDate = (txtHolidayDate.Text == "") ? DateTime.MinValue : Convert.ToDateTime(txtHolidayDate.Text);
            objHoliday.ClientID = Convert.ToInt32(ddlClient.SelectedValue);
            objHoliday.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            string errorMsg = string.Empty;
            if (TryUpdateModel(objHoliday, new FormValueProvider(ModelBindingExecutionContext)))
            {
                //Validate Name and Code at Insertion time 
                if (objHoliday.ID == 0)
                {
                    //Check Associate Name is Exist or Not
                    if (clsHoliday.ValidateHolidayNameIsExistorNot(txtHolidayName.Text))
                    {
                        errorMsg = "Holiday Name is Already Exist";
                    }
                    if (objHoliday.HolidayDate == DateTime.MinValue)
                    {
                        errorMsg = errorMsg + "<br>Holiday Date is required.";
                    }
                    if (objHoliday.ClientID == 0)
                        errorMsg = errorMsg + "<br>Please select Client.";
                    if (objHoliday.FinancialYear == 0)
                        errorMsg = errorMsg + "<br>The Financial Year is required.";
                }
                if (string.IsNullOrEmpty(errorMsg))
                {
                    int result = clsHoliday.SaveHoliday(objHoliday, Convert.ToInt32(Session["UID"]));
                    if (result > 0)
                    {
                        string msg = null;
                        if (objHoliday.ID == 0)
                        {
                            msg = "Successfully Inserted";
                        }
                        else
                        {
                            msg = "Successfully Updated";
                        }
                        GetHolidays();
                    }
                    else
                        Response.Write("Not Inserted");
                }
                else
                {
                    //Added my custome error to validation summary
                    Page currentPage = HttpContext.Current.Handler as Page;
                    currentPage.Validators.Add(new ValidationError(errorMsg));
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
                }
            }
            else
            {
                Page currentPage = HttpContext.Current.Handler as Page;
                currentPage.Validators.Add(new ValidationError(errorMsg));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
            }
        }

        protected void EditHoliday(object sender, EventArgs e)
        {
            clsHoliday objHoliday = new clsHoliday();
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            hdnHodId.Value = ((HiddenField)row.FindControl("hdnHolidayId")).Value;

            int holidayID = Convert.ToInt32(hdnHodId.Value);
            objHoliday = clsHoliday.GetHolidayByID(holidayID);

            //hdnBranchID.Value = branchId;
            txtYear.Text = objHoliday.FinancialYear.ToString();
            txtHolidayName.Text = objHoliday.HolidayName;
            txtHolidayDate.Text = objHoliday.HolidayDate.ToString("yyyy-MM-dd");
            LoadAllClients();
            ddlClient.Items.FindByValue(objHoliday.ClientID.ToString()).Selected = true;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

            //ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#holidayModal').modal()", true);//show the modal
        }

        protected void DeleteHoliday(object sender, EventArgs e)
        {
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            HiddenField hdnHolidayID = (HiddenField)row.FindControl("hdnHolidayId");
            int holidayID = Convert.ToInt32(hdnHolidayID.Value);  //stored Holiday ID
            int result = clsHoliday.RemoveHoliday(holidayID, Convert.ToInt32(Session["UID"]));
            if (result > 0)
            {
                //Response.Redirect("~/Masters/Holiday.aspx");
                GetHolidays();
            }
            else
            {
                Response.Write("Not Deleted");
            }
        }
        protected void AddHoliday(object sender, EventArgs e)
        {
            ClearAllFields();
            LoadAllClients();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
        }

        private void ClearAllFields()
        {
            txtHolidayDate.Text = "";
            txtHolidayName.Text = "";
            txtYear.Text = "";
        }

        private void LoadAllClients()
        {
            DataSet dataSet = clsClient.GetClientNameAndID(Convert.ToInt32(Session["CompanyID"]));
            ddlClient.DataSource = dataSet;
            ddlClient.DataTextField = "ClientName";
            ddlClient.DataValueField = "ID";
            ddlClient.DataBind();
        }

        protected void ddlClient_DataBound(object sender, EventArgs e)
        {
            ddlClient.Items.Insert(0, new ListItem("--Select Client--", "0"));
        }
    }
}