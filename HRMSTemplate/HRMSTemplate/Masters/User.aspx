﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="HRMSTemplate.Masters.User" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div>
                        <p class="card-title">
                            User Details
                            <span style="float: right">
                                <asp:Button ID="Button3" runat="server" Text="Add User" CssClass="btn btn-primary" CausesValidation="false" OnClick="AddUser" />
                            </span>
                        </p>
                    </div>

                    <%-- <div class="panel-heading">
                        <div class="row">
                            <h3>User Details</h3>
                            <span>
                                <asp:Button ID="Button1" runat="server" Text="Add User" CssClass="pull-right mybutton btn btn-primary" CausesValidation="false" OnClick="AddUser" />
                                <%--<input type="button" class=" pull-right mybutton btn btn-primary" onclick="showClientModal()" value="Add Client" />
                            </span>
                        </div>
                    </div>--%>
                    <div class="row">
                        <div class="card-body">
                            <div class="table-responsive">

                                <asp:GridView ID="grdUser" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User Name">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnUserId" runat="server" Value='<%# Eval("UserID") %>' />
                                                <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("LoginName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Parent User Role">
                                            <ItemTemplate>
                                                <asp:Label ID="lblParentUserRole" runat="server" Text='<%# Eval("ParentRoleName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User Role">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnUserRoleID" runat="server" Value='<%# Eval("User_RolesId") %>' />
                                                <asp:Label ID="lblRoleName" runat="server" Text='<%# Eval("RoleName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Unit Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCompanyName" runat="server" Text='<%# Eval("CompanyName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Client Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClientName" runat="server" Text='<%# Eval("ClientName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Associate Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAssociateName" runat="server" Text='<%# Eval("AssociateName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkCode" runat="server" OnClientClick="return DeleteUser()">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Module Access">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkModuleView" runat="server" OnClick="lnkModuleView_Click" CommandArgument=' <%#Eval("LoginName") %>'>View</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Records
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-md-6">
                                    <asp:HiddenField ID="hdnUsrId" runat="server" />
                                    <span class="form-control-sm" style="margin-left: -10px;">User Name:</span>
                                    <asp:TextBox ID="txtUserName" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Password:</span>
                                    <asp:TextBox ID="txtPassword" CssClass="form-control form-control-sm" runat="server" TextMode="Password"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Parent User Role:</span>
                                    <asp:DropDownList ID="ddlParentUserRole" AutoPostBack="true" DataTextField="role_name" DataValueField="User_RolesId" runat="server" OnDataBound="ddlParentUser_DataBound" CssClass="form-control" OnSelectedIndexChanged="ddlParentUserRole_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">User Role:</span>
                                    <asp:DropDownList ID="ddlUserRole" DataTextField="role_name" DataValueField="User_RolesId" runat="server" OnDataBound="ddlUserRole_DataBound" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Client:</span>
                                    <asp:DropDownList ID="ddlClient" runat="server" CssClass="form-control" DataTextField="ClientName" DataValueField="ID" OnDataBound="ddlClient_DataBound"></asp:DropDownList>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Associate:</span>
                                    <asp:DropDownList ID="ddlAssociate" runat="server" CssClass="form-control" DataTextField="AssociateName" DataValueField="ID" OnDataBound="ddlAssociate_DataBound"></asp:DropDownList>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Employee:</span>
                                    <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="form-control" DataTextField="EmployeeName" DataValueField="ID" OnDataBound="ddlEmployee_DataBound"></asp:DropDownList>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnSaveUser" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveUser_Click" />
                    <button id="btnClose" type="button" class="btn btn-default" data-dismiss="modal" onclick="CloseUserModal()">Close</button>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">

        function DeleteUser() {
            if (confirm("Are you sure you want to delete this User?")) {
                return true;
            } else {
                return false;
            }
        }

        $(function () {
            $("#btnClose").click(function () {
                $("#userModal").modal('hide');
            });
        });

        function ShowPopup() {
            $("#userModal").modal("show");
        }

        function ApplyCss() {
            $("#<%=ddlParentUserRole.ClientID%>").select2({
                //placeholder: "Select Item",
                allowClear: true
            });

            $("#<%=ddlUserRole.ClientID%>").select2({
                //placeholder: "Select Item",
                allowClear: true
            });

            $("#<%=ddlAssociate.ClientID%>").select2({
                //placeholder: "Select Item",
                allowClear: true
            });
            $("#<%=ddlClient.ClientID%>").select2({
                //placeholder: "Select Item",
                allowClear: true
            });
        }

        $(document).ready(function () {
            ApplyCss();
        });
    </script>
</asp:Content>
