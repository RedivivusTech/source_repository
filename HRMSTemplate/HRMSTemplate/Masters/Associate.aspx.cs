﻿using HRMSTemplate.App_Code.BAL;
using HRMSTemplate.ValidationManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMSTemplate.Masters
{
    public partial class Associate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            var data = clsAssociate.GetAll(Convert.ToInt32(Session["CompanyID"]));
            grdAssociate.DataSource = data;
            grdAssociate.DataBind();
        }
        protected void btnSaveAssociate_Click(object sender, EventArgs e)
        {
            clsAssociate objAssociate = new clsAssociate();
            string errorMsg = string.Empty;
            objAssociate.CompanyID = Convert.ToInt32(Session["CompanyID"]);
            objAssociate.AssociateID = (hdnAsId.Value == "") ? 0 : Convert.ToInt32(hdnAsId.Value);
            objAssociate.AssociateCode = txtAssociateCode.Text;
            objAssociate.AssociateName = txtAssociateName.Text;
            objAssociate.City = txtCity.Text;
            objAssociate.State = txtState.Text;
            objAssociate.Address_line1 = txtAddress1.Text;
            objAssociate.Address_line2 = txtAddress2.Text;
            objAssociate.Country = txtCountry.Text;
            objAssociate.Pincode = (txtPincode.Text == "") ? 0 : Convert.ToInt32(txtPincode.Text);
            if (TryUpdateModel(objAssociate, new FormValueProvider(ModelBindingExecutionContext)))
            {
                //Validate Name and Code at Insertion time 
                if (objAssociate.AssociateID == 0)
                {
                    //Check Associate Name is Exist or Not
                    if (clsAssociate.ValidateAssociateIsExistorNot(txtAssociateName.Text))
                    {
                        errorMsg = "Associate Name is Already Exist";
                    }
                    if (clsAssociate.ValidateAssociateCodeIsExistorNot(txtAssociateCode.Text))
                    {
                        if (errorMsg == String.Empty)
                        {
                            errorMsg = "Associate Code is Already Exist";
                        }
                        else
                        {
                            errorMsg = errorMsg + "<br> Associate Code is Already Exist";
                        }
                    }
                }
                if (string.IsNullOrEmpty(errorMsg))
                {
                    int result = clsAssociate.SaveAssociate(objAssociate, Convert.ToInt32(Session["UID"]));
                    if (result > 0)
                    {
                        string msg = null;
                        if (objAssociate.AssociateID == 0)
                        {
                            msg = "Successfully Inserted";
                        }
                        else
                        {
                            msg = "Successfully Updated";
                        }
                        string title = "Done";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Success", "<script>showpop5('" + msg + "','" + title + "')</script>", true);
                        BindData();
                    }
                    else
                        Response.Write("Not Inserted");
                }
                else
                {
                    //Added my custome error to validation summary
                    Page currentPage = HttpContext.Current.Handler as Page;
                    currentPage.Validators.Add(new ValidationError(errorMsg));
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
            }

        }
        protected void EditAssociate(object sender, EventArgs e)
        {

            clsAssociate objAssociate = new clsAssociate();
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            hdnAsId.Value = ((HiddenField)row.FindControl("hdnAssociateId")).Value;
            int associateId = Convert.ToInt32(hdnAsId.Value);
            objAssociate = clsAssociate.GetAssociateByID(associateId);
            //hdnBranchID.Value = branchId;
            txtAssociateCode.Text = objAssociate.AssociateCode;
            txtAssociateName.Text = objAssociate.AssociateName;
            txtAddress1.Text = objAssociate.Address_line1;
            txtAddress2.Text = objAssociate.Address_line2;
            txtCity.Text = objAssociate.City;
            txtState.Text = objAssociate.State;
            txtCountry.Text = objAssociate.Country;
            txtPincode.Text = Convert.ToString(objAssociate.Pincode);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);

        }

        protected void DeleteAssociate(object sender, EventArgs e)
        {
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            HiddenField hdnBranchID = (HiddenField)row.FindControl("hdnBranchId");
            int associateID = Convert.ToInt32(hdnBranchID.Value);  //stored Branch ID
            int result = clsAssociate.RemoveAssociate(associateID, Convert.ToInt32(Session["UID"]));
            if (result > 0)
            {
                Response.Redirect("Associate.aspx");
            }
            else
            {
                Response.Write("Not Deleted");
            }

        }

        protected void btnAddAssociate_Click(object sender, EventArgs e)
        {
            ClearAllFields();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
        }

        private void ClearAllFields()
        {
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtAssociateCode.Text = "";
            txtAssociateName.Text = "";
            txtCity.Text = "";
            txtCountry.Text = "";
            txtPincode.Text = "";
            txtState.Text = "";
        }
    }
}