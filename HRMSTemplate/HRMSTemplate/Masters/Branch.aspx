﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Branch.aspx.cs" Inherits="HRMSTemplate.Masters.Branch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div>
                        <p class="card-title">
                            Branch Details
                            <span style="float: right">
                                <asp:Button ID="Button1" runat="server" Text="Add Branch" CssClass="btn btn-primary" CausesValidation="false" OnClick="btnAddBranch_Click" />

                                <%--<input type="button" class="btn btn-primary" onclick="showAssociateModal()" value="Add Associate" />--%></span>
                        </p>
                    </div>
                    <%-- <div class="panel-heading">
                        <div class="row">
                            <h3>Branch Details</h3>
                            <span>
                                <asp:Button ID="btnAddBranch" runat="server" Text="Add Branch" CssClass="pull-right mybutton btn btn-primary" CausesValidation="false" OnClick="btnAddBranch_Click" />
                                <%--<input type="button" class=" pull-right mybutton btn btn-primary" onclick="showModal()" value="Add Branh" />
                            </span>
                        </div>
                    </div>--%>
                    <div class="row">
                        <div class="card-body">
                            <div class="table-responsive">

                                <asp:GridView ID="grdBranch" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Branch Code">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnBranchId" runat="server" Value='<%# Eval("ID") %>' />
                                                <asp:Label ID="lblBranchCode" runat="server" Text='<%# Eval("BranchCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Branch Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBranchName" runat="server" Text='<%# Eval("BranchName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address_line1">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress1" runat="server" Text='<%# Eval("Address_line1") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address_line2">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress2" runat="server" Text='<%# Eval("Address_line2") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" runat="server" Text='<%# Eval("City") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State">
                                            <ItemTemplate>
                                                <asp:Label ID="lblState" runat="server" Text='<%# Eval("State") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Country">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("Country") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pincode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPincode" runat="server" Text='<%# Eval("Pincode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" OnClick="EditBranch">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkCode" runat="server" OnClientClick="return DeleteBranch()" OnClick="DeleteBranch">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Records

                                    </EmptyDataTemplate>

                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLabel">Branch</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <asp:HiddenField ID="hdnBrId" runat="server" />
                                    <span class="form-control-sm" style="margin-left: -10px;">Code:<span class="text-danger"> *</span></span>

                                    <asp:TextBox ID="txtBranchCode" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Name:<span class="text-danger"> *</span></span>
                                    <asp:TextBox ID="txtBranchName" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">City:<span class="text-danger"> *</span></span>
                                    <asp:TextBox ID="txtCity" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">State:</span>
                                    <asp:TextBox ID="txtState" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Country:</span>
                                    <asp:TextBox ID="txtCountry" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Pincode:</span>
                                    <asp:TextBox ID="txtPincode" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Address1:</span>
                                    <asp:TextBox ID="txtAddress1" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server" TextMode="MultiLine"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Address2:</span>
                                    <asp:TextBox ID="txtAddress2" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <asp:ValidationSummary ID="validationSummary" runat="server" ShowModelStateErrors="true" DisplayMode="List" ForeColor="Red" />
                            </div>
                            <asp:Button ID="btnSabeBranch" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveBranch_Click" />
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        <%--</ContentTemplate>
    </asp:UpdatePanel>--%>

    <script type="text/javascript">
        function ShowPopup() {
            $("#myModal").modal("show");
        }

        function DeleteBranch() {
            if (confirm("Are you sure you want to delete this Branch?")) {
                return true;
            } else {
                return false;
            }
        }
    </script>
</asp:Content>
