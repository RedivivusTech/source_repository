﻿using HRMSTemplate.App_Code.BAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HRMSTemplate.Masters
{
    public partial class Company : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet ds = clsCompany.GetAll();
                grdCompany.DataSource = ds;
                grdCompany.DataBind();
            }
        }

        protected void EditCompany(object sender, EventArgs e)
        {
            clsCompany objCompany = new clsCompany();
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            hdnComId.Value = ((HiddenField)row.FindControl("hdnCompanyId")).Value;

            int CompanyID = Convert.ToInt32(hdnComId.Value);
            objCompany = clsCompany.GetCompanyByID(CompanyID);

            txtCompanyCode.Text = objCompany.CompanyCode;
            txtCompanyName.Text = objCompany.CompanyName;
            txtRegistrationNo.Text = objCompany.Registration_No;
            txtGST_No.Text = objCompany.GST_No;
            txtPan_No.Text = objCompany.Pan_No;
            txtDivision.Text = objCompany.Division;
            txtAddressline1.Text = objCompany.Address_line1;
            txtAddressline2.Text = objCompany.Address_line2;
            txtCity.Text = objCompany.City;
            txtPincode.Text = Convert.ToString(objCompany.Pincode);
            txtEMailID.Text = objCompany.EMailID;
            txtContactNo.Text = objCompany.ContactNo;
            txtCountry.Text = objCompany.Country;
            txtState.Text = objCompany.State;
            //ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#CompanyModal').modal()", true);//show the modal
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { ShowPopup(); });", true);
        }

        protected void DeleteCompany(object sender, EventArgs e)
        {
            LinkButton linkbutton = (LinkButton)sender;
            GridViewRow row = (GridViewRow)linkbutton.NamingContainer;
            HiddenField hdnCompanyID = (HiddenField)row.FindControl("hdnCompanyId");
            int companyID = Convert.ToInt32(hdnCompanyID.Value);  //stored Branch ID
            int result = clsCompany.RemoveCompany(companyID, Convert.ToInt32(Session["UID"]));
            if (result > 0)
            {
                Response.Redirect("Company.aspx");
            }
            else
            {
                Response.Write("Not Deleted");
            }
        }

        protected void btnSaveCompany_Click(object sender, EventArgs e)
        {
            clsCompany objCompany = new clsCompany();

            if (TryUpdateModel(objCompany, new FormValueProvider(ModelBindingExecutionContext)))
            {
                objCompany.CompanyID = (hdnComId.Value == "") ? 0 : Convert.ToInt32(hdnComId.Value);
                objCompany.CompanyCode = txtCompanyCode.Text;
                objCompany.CompanyName = txtCompanyName.Text;
                objCompany.Registration_No = txtRegistrationNo.Text;
                objCompany.GST_No = txtGST_No.Text;
                objCompany.City = txtCity.Text;
                objCompany.State = txtState.Text;
                objCompany.Address_line1 = txtAddressline1.Text;
                objCompany.Address_line2 = txtAddressline2.Text;
                objCompany.Pan_No = txtPan_No.Text;
                objCompany.Pincode = Convert.ToInt32(txtPincode.Text);
                objCompany.Division = txtDivision.Text;
                objCompany.EMailID = txtEMailID.Text;
                objCompany.ContactNo = txtContactNo.Text;
                objCompany.Country = txtCountry.Text;
                int result = clsCompany.SaveCompany(objCompany, Convert.ToInt32(Session["UID"]));
                if (result > 0)
                {
                    Response.Redirect("Company.aspx");
                }
                else
                    Response.Write("Not Inserted");
            }
            else
            {
                ShowMessage(objCompany.ToString());
                ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#CompanyModal').modal()", true);//show the modal
            }
        }
        public void ShowMessage(string msg)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('" + msg + "');</script>");
        }
        //protected void AddCompany(object sender, EventArgs e)
        //{
        //    ClearAllFields();
        //    ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "$('#CompanyModal').modal()", true);//show the modal
        //}

        private void ClearAllFields()
        {
            txtAddressline1.Text = "";
            txtAddressline2.Text = "";
            txtCity.Text = "";
            txtCompanyCode.Text = "";
            txtCompanyName.Text = "";
            txtContactNo.Text = "";
            txtCountry.Text = "";
            txtDivision.Text = "";
            txtEMailID.Text = "";
            txtGST_No.Text = "";
            txtPan_No.Text = "";
            txtPincode.Text = "";
            txtRegistrationNo.Text = "";
            txtState.Text = "";
        }
    }
}