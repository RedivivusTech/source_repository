﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Company.aspx.cs" Inherits="HRMSTemplate.Masters.Company" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div>
                        <p class="card-title">
                            Company Details
                            <span style="float: right">
                                <input type="button" class="btn btn-primary" onclick="showComapnyModal()" value="Add Company" />
                                <%--<asp:Button ID="Button1" runat="server" Text="Add Company" CssClass="btn btn-primary" CausesValidation="false" OnClick="AddCompany" />--%>

                            </span>
                        </p>
                    </div>
                    <%--<div class="panel-heading">
                        <div class="row">
                            <h3>Company Details</h3>
                            <span>
                                <asp:Button ID="Button1" runat="server" Text="Add Company" CssClass="pull-right mybutton btn btn-primary" CausesValidation="false" OnClick="AddCompany" />
                                <%--<input type="button" class=" pull-right mybutton btn btn-primary" onclick="showClientModal()" value="Add Client" />
                            </span>
                        </div>
                    </div>--%>
                    <div class="row">
                        <div class="card-body">
                            <div class="table-responsive">
                                <asp:GridView ID="grdCompany" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Company Code ">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnCompanyId" runat="server" Value='<%# Eval("ID") %>' />
                                                <asp:Label ID="lblCompanyCode" runat="server" Text='<%# Eval("CompanyCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Company Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCompanyName" runat="server" Text='<%# Eval("CompanyName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ContactNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblContactNo" runat="server" Text='<%# Eval("ContactNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EMail ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEMailID" runat="server" Text='<%# Eval("EMailID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" runat="server" Text='<%# Eval("City") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State">
                                            <ItemTemplate>
                                                <asp:Label ID="lblState" runat="server" Text='<%# Eval("State") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Country">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("Country") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Address line1">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddressline1" runat="server" Text='<%# Eval("Address_line1") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address line2">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddressline2" runat="server" Text='<%# Eval("Address_line2") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" OnClick="EditCompany">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkCode" runat="server" OnClientClick="return DeleteCompany()" OnClick="DeleteCompany">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Records
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="CompanyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Company</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <asp:ValidationSummary ID="validationSummary" runat="server" ShowModelStateErrors="true" DisplayMode="List" ForeColor="Red" />
                    </div>
                    <div class="row">
                        <asp:HiddenField ID="hdnComId" runat="server" />
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Company Code:</span>
                            <asp:TextBox ID="txtCompanyCode" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Company Name:</span>
                            <asp:TextBox ID="txtCompanyName" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <%-- <div class="col-md-6">
                                        <label>Tax ID:</label>
                                        <asp:TextBox ID="txtTaxID" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>--%>

                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Registration No:</span>
                            <asp:TextBox ID="txtRegistrationNo" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>

                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">GST No:</span>
                            <asp:TextBox ID="txtGST_No" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">PAN No:</span>
                            <asp:TextBox ID="txtPan_No" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Division:</span>
                            <asp:TextBox ID="txtDivision" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>

                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">City:</span>
                            <asp:TextBox ID="txtCity" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Pincode:</span>
                            <asp:TextBox ID="txtPincode" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Email ID:</span>
                            <asp:TextBox ID="txtEMailID" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server" TextMode="Email"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">ContactNo:</span>
                            <asp:TextBox ID="txtContactNo" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server" TextMode="Phone"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Country:</span>
                            <asp:TextBox ID="txtCountry" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">State:</span>
                            <asp:TextBox ID="txtState" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Address_line1:</span>
                            <asp:TextBox ID="txtAddressline1" ClientIDMode="Static" CssClass="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Address_line2:</span>
                            <asp:TextBox ID="txtAddressline2" ClientIDMode="Static" CssClass="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnSaveCompany" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveCompany_Click" />
                    <button id="btnClose" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        function DeleteCompany() {
            if (confirm("Are you sure you want to delete this Company?")) {
                return true;
            } else {
                return false;
            }
        }

        $(function () {
            $("#btnClose").click(function () {
                $("#CompanyModal").modal('hide');
            });
        });

        function ShowPopup() {
            $("#CompanyModal").modal("show");
        }
        function showComapnyModal() {
            $('#txtAddressline1').val("");
            $('#txtAddressline2').val("");
            $('#txtCity').val("");
            $('#txtCompanyCode').val("");
            $('#txtCompanyName').val("");
            $('#txtContactNo').val("");
            $('#txtCountry').val("");
            $('#txtDivision').val("");
            $('#txtEMailID').val("");
            $('#txtGST_No').val("");
            $('#txtPan_No').val("");
            $('#txtPincode').val("");
            $('#txtRegistrationNo').val("");
            $('#txtState').val("");
            $('#CompanyModal').modal('show');
        }
    </script>
</asp:Content>
