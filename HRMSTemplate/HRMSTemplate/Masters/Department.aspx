﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Department.aspx.cs" Inherits="HRMSTemplate.Masters.Department" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div>
                        <p class="card-title">
                            Department Details
                            <span style="float: right">
                                <%--<input type="button" class="btn btn-primary" onclick="showDepartmentModal()" value="Add Department" />--%>
                                <asp:Button ID="Button1" runat="server" Text="Add Department" CssClass="btn btn-primary" CausesValidation="false" OnClick="AddDepartment" />
                            </span>
                        </p>
                    </div>
                    <%--<div class="row">
                <h3>Department Details</h3>
                <span>
                    <asp:Button ID="Button1" runat="server" Text="Add Department" CssClass="pull-right mybutton btn btn-primary" CausesValidation="false" OnClick="AddDepartment" />
                    <%--<input type="button" class=" pull-right mybutton btn btn-primary" onclick="showClientModal()" value="Add Client" />
                </span>
            </div>--%>

                    <div class="row">
                        <div class="card-body">
                            <div class="table-responsive">

                                <asp:GridView ID="grdDepartment" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Department Code">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnDepartmentId" runat="server" Value='<%# Eval("ID") %>' />
                                                <asp:Label ID="lblDepatCode" runat="server" Text='<%# Eval("DepartmentCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Department Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClientName" runat="server" Text='<%# Eval("DepartmentName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Parent Department Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRegNo" runat="server" Text='<%# Eval("ParentDeptName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" OnClick="EditDepartment">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="return DeleteDepartment()" OnClick="DeleteDepartment">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Records

                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>
    <div class="modal fade" id="deptModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Department</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <asp:HiddenField ID="hdnDeptsId" runat="server" />
                            <span class="form-control-sm" style="margin-left: -10px;">Code:<span class="text-danger"> *</span> </span>
                            <asp:TextBox ID="txtDepartmentCode" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Name:<span class="text-danger"> *</span> </span>
                            <asp:TextBox ID="txtDepartmentName" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Parent Department:<span class="text-danger"> *</span> </span>
                            <asp:DropDownList ID="ddlParentDepartment" DataTextField="DepartmentName" DataValueField="ID" runat="server" OnDataBound="ddlParentDepartment_DataBound" CssClass="form-control" Width="100%">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <span>
                        <asp:ValidationSummary ID="validationSummary" ClientIDMode="Static" runat="server" ShowModelStateErrors="true" DisplayMode="List" ForeColor="Red" />
                    </span>
                    <asp:Button ID="btnSaveDepartment" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveDepartment_Click" />
                    <button id="btnClose" type="button" class="btn btn-default" data-dismiss="modal" onclick="CloseDepartmentModal()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>


    <script type="text/javascript">

        function DeleteDepartment() {
            if (confirm("Are you sure you want to delete this Department?")) {
                return true;
            } else {
                return false;
            }
        }

        function showDepartmentModal() {
            $('#txtDepartmentCode').val("");
            $('#txtDepartmentName').val("");
            $('#deptModal').modal('show');
        }

        function ShowPopup() {
            $("#deptModal").modal("show");
        }

        $(document).ready(function () {
            $("#<%=ddlParentDepartment.ClientID%>").select2({
                //placeholder: "Select Item",
                allowClear: true
            });
        });
    </script>
</asp:Content>
