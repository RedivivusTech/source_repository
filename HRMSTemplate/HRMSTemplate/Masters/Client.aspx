﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Client.aspx.cs" Inherits="HRMSTemplate.Masters.Client" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenu" runat="server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div>
                        <p class="card-title">
                            Client Details
                            <span style="float: right">
                                <%--<input type="button" class="mybutton btn btn-primary" onclick="showClientModal()" value="Add Client" />--%>
                                <asp:Button ID="Button2" runat="server" Text="Add Client" CssClass="mybutton btn btn-primary" CausesValidation="false" OnClick="AddClient" />
                            </span>
                        </p>
                    </div>
                    <%--<div class="panel-heading">
                        <div class="row">
                            <h3>Client Details</h3>
                            <span>
                                <asp:Button ID="Button1" runat="server" Text="Add Client" CssClass="pull-right mybutton btn btn-primary" CausesValidation="false" OnClick="AddClient" />
                                <%--<input type="button" class=" pull-right mybutton btn btn-primary" onclick="showClientModal()" value="Add Client" />
                            </span>
                        </div>
                    </div>--%>
                    <div class="row">
                        <div class="card-body">
                            <div class="table-responsive">
                                <asp:GridView ID="grdClient" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Client Code">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnClientId" runat="server" Value='<%# Eval("ID") %>' />
                                                <asp:Label ID="lblClientCode" runat="server" Text='<%# Eval("ClientCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Client Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClientName" runat="server" Text='<%# Eval("ClientName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Registration No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRegNo" runat="server" Text='<%# Eval("Registration_No") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Email_ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmailID" runat="server" Text='<%# Eval("EMailID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No of Employee">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNoOfEmp" runat="server" Text='<%# Eval("No_of_Employees") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Division">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDivision" runat="server" Text='<%# Eval("Division") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress1" runat="server" Text='<%# Eval("Address_line1") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Address2">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress2" runat="server" Text='<%# Eval("Address_line2") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" runat="server" Text='<%# Eval("City") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State">
                                            <ItemTemplate>
                                                <asp:Label ID="lblState" runat="server" Text='<%# Eval("State") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Country">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("Country") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" OnClick="EditClient">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkCode" runat="server" OnClientClick="return DeleteClient()" OnClick="DeleteClient">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Records

                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="modal fade" id="clientModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLabel">Client</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <asp:HiddenField ID="hdnClsId" runat="server" />
                                    <span class="form-control-sm" style="margin-left: -10px;">Client Code:</span>
                                    <asp:TextBox ID="txtClientCode" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Client Name:</span>
                                    <asp:TextBox ID="txtClientName" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Parent Client:</span>
                                    <asp:DropDownList ID="ddlParentClient" DataTextField="ClientName" DataValueField="ID" runat="server" OnDataBound="ddlParentClient_DataBound" CssClass="form-control form-control-sm">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Branch:</span>
                                    <asp:DropDownList ID="ddlBranch" DataTextField="BranchName" DataValueField="ID" runat="server" OnDataBound="ddlBranch_DataBound" CssClass="form-control form-control-sm">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Registration No:</span>
                                    <asp:TextBox ID="txtRegNo" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">No Of Employee:</span>
                                    <asp:TextBox ID="txtNoOfEmp" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">PAN No:</span>
                                    <asp:TextBox ID="txtPANNo" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">GST No:</span>
                                    <asp:TextBox ID="txtGSTNo" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Division:</span>
                                    <asp:TextBox ID="txtDivision" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Email ID:</span>
                                    <asp:TextBox ID="txtEmail" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Contact No:</span>
                                    <asp:TextBox ID="txtContact" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Pincode:</span>
                                    <asp:TextBox ID="txtPincode" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Address 1:</span>
                                    <asp:TextBox ID="txtAddress1" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server" TextMode="MultiLine"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Address 2:</span>
                                    <asp:TextBox ID="txtAddress2" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server" TextMode="MultiLine"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">City:</span>
                                    <asp:TextBox ID="txtCity" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">State:</span>
                                    <asp:TextBox ID="txtState" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6">
                                    <span class="form-control-sm" style="margin-left: -10px;">Country:</span>
                                    <asp:TextBox ID="txtCountry" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSaveClient" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveClient_Click" />
                            <button id="btnClose" type="button" class="btn btn-default" data-dismiss="modal" onclick="CloseClientModal()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">

        function DeleteClient() {
            if (confirm("Are you sure you want to delete this Client?")) {
                return true;
            } else {
                return false;
            }
        }

        //function showClientModal() {
        //    $('#txtClientCode').val("");
        //    $('#txtClientName').val("");
        //    $('#txtRegNo').val("");
        //    $('#txtNoOfEmp').val("");
        //    $('#txtPANNo').val("");
        //    $('#txtGSTNo').val("");
        //    $('#txtDivision').val("");
        //    $('#txtEmail').val("");
        //    $('#txtContact').val("");
        //    $('#txtPincode').val("");
        //    $('#txtAddress1').val("");
        //    $('#txtAddress2').val("");
        //    $('#txtCity').val("");
        //    $('#txtState').val("");
        //    $('#txtCountry').val("");
        //    $('#clientModal').modal('show');
        //}

        function ShowPopup() {
            $("#clientModal").modal("show");
        }

        $(function () {
            $("#btnClose").click(function () {
                $("#clientModal").modal('hide');
            });
        });

        $(document).ready(function () {
            $("#<%=ddlParentClient.ClientID%>").select2({
                //placeholder: "Select Item",
                allowClear: true
            });

            $("#<%=ddlBranch.ClientID%>").select2({
                //placeholder: "Select Item",
                allowClear: true
            });
        });
    </script>

</asp:Content>
