﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FinancialYear.aspx.cs" Inherits="HRMSTemplate.Masters.FinancialYear" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div>
                        <p class="card-title">
                            Financial Year Details
                            <span style="float: right">
                                <asp:Button ID="btnAddYear" runat="server" Text="Add Year" CssClass="btn btn-primary" OnClick="AddFinancialYear" />
                                <%--<input type="button" class="btn btn-primary" onclick="showAssociateModal()" value="Add Associate" />--%></span>
                        </p>
                    </div>

                    <%--<div class="panel-heading">
                        <div class="row">
                            <h3>Financial Year Details</h3>
                            <span>
                                <asp:Button ID="Button1" runat="server" Text="Add Year" CssClass="pull-right mybutton btn btn-primary" OnClick="AddFinancialYear" />
                            </span>
                        </div>
                    </div>--%>

                    <div class="row">
                        <div class="card-body">
                            <div class="table-responsive">
                                <asp:GridView ID="grdFinancialYear" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Financial Year">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnyearId" runat="server" Value='<%# Eval("ID") %>' />
                                                <asp:Label ID="lblFinancialYear" runat="server" Text='<%# Eval("FinancialYear") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Year Start">
                                            <ItemTemplate>
                                                <asp:Label ID="lblYearStart" runat="server" Text='<%# Eval("YearStart") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Year End">
                                            <ItemTemplate>
                                                <asp:Label ID="lblYearEnd" runat="server" Text='<%# Eval("YearEnd") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" OnClick="EditYear">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkCode" runat="server" OnClientClick="return DeleteYear()" OnClick="DeleteYear">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Records

                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="financialYearModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Financial Year</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <asp:HiddenField ID="hdnFinancialYearId" runat="server" />
                            <span class="form-control-sm" style="margin-left: -10px;">Financial Year:</span>
                            <asp:TextBox ID="txtYear" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Year Start:</span>
                            <asp:TextBox ID="txtStartDate" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server" TextMode="Date"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Year End:</span>
                            <asp:TextBox ID="txtEndDate" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server" TextMode="Date"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnSaveYear" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveYear_Click" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">


        function ShowPopup() {
            $("#financialYearModal").modal("show");
        }

        function DeleteYear() {
            if (confirm("Are you sure you want to delete this Financial Year?")) {
                return true;
            } else {
                return false;
            }
        }
    </script>
</asp:Content>
