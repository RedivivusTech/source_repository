﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeLeave.aspx.cs" Inherits="HRMSTemplate.Masters.EmployeeLeave" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div>
                        <p class="card-title">
                            Employee Leave Details
                            <span style="float: right">
                                <asp:Button ID="btnAddLeave" runat="server" Text="Apply Leave" CssClass="pull-right mybutton btn btn-primary" CausesValidation="false" OnClick="AddEmployeeLeave" />
                            </span>
                        </p>
                    </div>

                    <%--<div class="row">
                        <h3>Leave Details</h3>
                        <span>
                            <asp:Button ID="btnAddLeave" runat="server" Text="Add Leave" CssClass="pull-right mybutton btn btn-primary" CausesValidation="false" OnClick="AddLeave" />
                        </span>
                    </div>--%>

                    <div class="row">
                        <div class="card-body">
                            <div class="table-responsive">
                                <asp:GridView ID="grdEmployeeLeave" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Leave Name">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnLeaveRequestId" runat="server" Value='<%# Eval("ID") %>' />
                                                <asp:Label ID="lblLeaveName" runat="server" Text='<%# Eval("LeaveName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Short Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblShortName" runat="server" Text='<%# Eval("ShortName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No of Leaves">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNoOfLeaves" runat="server" Text='<%# Eval("NoOfLeaves") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Payment Ratio">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentRatio" runat="server" Text='<%# Eval("PaymentRatio") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Client Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClientName" runat="server" Text='<%# Eval("ClientName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkCode" runat="server" OnClientClick="return DeleteLeave()">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Records

                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="employeeLeaveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Employee Leave</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Leave Type:</span>
                            <asp:DropDownList ID="ddlLeaveType" ClientIDMode="Static" DataTextField="LeaveName" DataValueField="LeaveMID" runat="server" OnDataBound="ddlLeaveType_DataBound" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-6">
                            <asp:HiddenField ID="hdnLeaveReqId" runat="server" />
                            <span class="form-control-sm" style="margin-left: -10px;">Available Leave:</span>
                            <asp:TextBox ID="txtAvailableLeave" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server" ReadOnly="true"></asp:TextBox>
                        </div>

                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">From Date:</span>
                            <asp:TextBox ID="txtFromDate" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server" TextMode="Date"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">To Date:</span>
                            <asp:TextBox ID="txtToDate" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server" TextMode="Date"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Leave Sub Type:</span>
                            <asp:DropDownList ID="ddlLeaveSubType" runat="server"></asp:DropDownList>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">No of Leave:</span>
                            <asp:TextBox ID="txtNoofLeave" ClientIDMode="Static" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Leave Reason:</span>
                            <asp:TextBox ID="txtLeaveReason" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <span class="form-control-sm" style="margin-left: -10px;">Leave Address:</span>
                            <asp:TextBox ID="txtLeaveAddress" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnSaveEmployeeLeave" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveEmployeeLeave_Click" />
                    <button id="btnClose" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">


        function ShowPopup() {
            $("#employeeLeaveModal").modal("show");
        }

        $(document).ready(function () {
            $("#<%=ddlLeaveType.ClientID%>").select2({
                allowClear: true
            });

            $("#<%=ddlLeaveSubType.ClientID%>").select2({
                allowClear: true
            });
        });
    </script>
</asp:Content>
