﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsAssociate
    {
        public int AssociateID { get; set; }
        [Required(ErrorMessage = "The Associate Code is required.")]
        public string AssociateCode { get; set; }
        [Required(ErrorMessage = "The Associate Name is required.")]
        public string AssociateName { get; set; }
        public string Address_line1 { get; set; }
        public string Address_line2 { get; set; }
        [Required(ErrorMessage = "The City is required.")]
        public string City { get; set; }
        public int Pincode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public int CompanyID { get; set; }


        public static DataSet GetAll(int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_ASSOCIATE_DETAILS_BY_COMPANY_ID", objParams);
        }

        public static clsAssociate GetAssociateByID(int associateID)
        {
            clsAssociate objAssociate = new clsAssociate();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@AssociateID", associateID);
            SqlDataReader reader = objSqlHelper.ExecuteReader("GET_ASSOCIATE_DETAILS_BY_ID", objParams);
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    objAssociate.AssociateID = associateID;
                    objAssociate.AssociateCode = reader["AssociateCode"].ToString();
                    objAssociate.AssociateName = reader["AssociateName"].ToString();
                    objAssociate.Address_line1 = reader["Address_line1"].ToString();
                    objAssociate.Address_line2 = reader["Address_line2"].ToString();
                    objAssociate.City = reader["City"].ToString();
                    objAssociate.State = reader["State"].ToString();
                    objAssociate.Country = Convert.ToString(reader["Country"]);
                    objAssociate.Pincode = Convert.ToInt32(reader["Pincode"]);
                }
            }
            return objAssociate;
        }

        public static DataSet GetAssociateNameAndID(int companyID)
        {
            clsAssociate objAssociate = new clsAssociate();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_AssociateNameAndID", objParams);
        }

        public static int SaveAssociate(clsAssociate objAssociate, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[11];

            objParams[0] = new SqlParameter("@AssociateID", objAssociate.AssociateID);
            objParams[1] = new SqlParameter("@AssociateCode", objAssociate.AssociateCode);
            objParams[2] = new SqlParameter("@AssociateName", objAssociate.AssociateName);
            objParams[3] = new SqlParameter("@Address_line1", objAssociate.Address_line1);
            objParams[4] = new SqlParameter("@City", objAssociate.City);
            objParams[5] = new SqlParameter("@State", objAssociate.State);
            objParams[6] = new SqlParameter("@CompanyID", objAssociate.CompanyID);
            objParams[7] = new SqlParameter("@CreatedBy", userID);
            objParams[8] = new SqlParameter("@Address_line2", objAssociate.Address_line2);
            objParams[9] = new SqlParameter("@Country", objAssociate.Country);
            objParams[10] = new SqlParameter("@Pincode", objAssociate.Pincode);


            return int.Parse(objSqlHelper.ExecuteNonQuery("Save_tbl_AssociateMaster", objParams).ToString());
        }

        public static int RemoveAssociate(int associateID, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@AssociateID", associateID);
            objParams[1] = new SqlParameter("@ModifiedBy", userID);

            return int.Parse(objSqlHelper.ExecuteNonQuery("ASSOCIATE_DETAILS_DELETE", objParams).ToString());
        }

        public static bool ValidateAssociateIsExistorNot(string uName) 
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@AssociateName", uName);
            return Convert.ToBoolean(objSqlHelper.ExecuteScalar("Validate_Associate_Exist_Or_Not", objParams).ToString());
        }

        public static bool ValidateAssociateCodeIsExistorNot(string associateCode) 
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@AssociateCode", associateCode);
            return Convert.ToBoolean(objSqlHelper.ExecuteScalar("Validate_AssociateCode_Exist_Or_Not", objParams).ToString());
        }
    }
}