﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsEmployeeLeave
    {
        public int ID { get; set; } 
        public int EmployeeID { get; set; }
        public int LeaveID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int NoOfLeave { get; set; }
        public string LeaveReason { get; set; }
        public string LeaveAddress { get; set; }
        public int CompanyID { get; set; }
        public int ClientID { get; set; }
        public int LeaveSubType { get; set; }


        public static int SaveEmployeeLeaves(clsEmployeeLeave objEmpLeaves, int userID) 
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[8];

            objParams[0] = new SqlParameter("@LeaveID", objEmpLeaves.LeaveID);
            objParams[1] = new SqlParameter("@EmployeeID", objEmpLeaves.EmployeeID);
            objParams[2] = new SqlParameter("@FromDate", objEmpLeaves.FromDate);
            objParams[3] = new SqlParameter("@ToDate", objEmpLeaves.ToDate);
            objParams[4] = new SqlParameter("@NoOfLeave", objEmpLeaves.NoOfLeave);
            objParams[5] = new SqlParameter("@LeaveReason", objEmpLeaves.LeaveReason);
            objParams[6] = new SqlParameter("@CreatedBy", userID);
            objParams[7] = new SqlParameter("@LeaveAddress", objEmpLeaves.LeaveAddress);


            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_EMPLOYEE_LEAVES_DETAILS", objParams).ToString());
        }
    }
}