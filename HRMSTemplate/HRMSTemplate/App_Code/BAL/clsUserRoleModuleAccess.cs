﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsUserRoleModuleAccess
    {
        public int ID { get; set; }
        public int UserRoleID { get; set; }
        public int ModuleID { get; set; }
        public int CompanyID { get; set; }
        public bool IsActive { get; set; }

        public static int SaveUserRoleModuleAccess(clsUserRoleModuleAccess objUserModuleAccess, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();

            SqlParameter[] objParams = new SqlParameter[5];
            objParams[0] = new SqlParameter("@UserRoleID", objUserModuleAccess.UserRoleID);
            objParams[1] = new SqlParameter("@ModuleID", objUserModuleAccess.ModuleID);
            objParams[2] = new SqlParameter("@CompanyID", objUserModuleAccess.CompanyID);
            objParams[3] = new SqlParameter("@CreatedBy", userID);
            objParams[4] = new SqlParameter("@IsActive", objUserModuleAccess.IsActive);
            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_USER_ROLE_MODULE_ACCESS", objParams).ToString());
        }

        public static DataTable GetByUserRoleID(int userRoleID, int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@UserRoleID", userRoleID);
            objParams[1] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataTable("GET_USER_ROLE_MODULE_ACCESS_BY_ID", objParams);
        }

        public static int CheckModuleAccessForPermission(int userRoleID, int moduleID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@UserRoleID", userRoleID);
            objParams[1] = new SqlParameter("@ModuleID", moduleID);
            return int.Parse(objSqlHelper.ExecuteScalar("CHECK_MODULE_ACCESS_FOR_PERMISSION", objParams).ToString());
        }


    }
}