﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsBranch
    {
        public int BranchID { get; set; }
        [Required(ErrorMessage = "The Branch Code is required.")]
        public string BranchCode { get; set; }
        [Required(ErrorMessage = "The Branch Name is required.")]
        public string BranchName { get; set; }
        public string Address_line1 { get; set; }
        public string Address_line2 { get; set; }
        [Required(ErrorMessage = "The City is required.")]
        public string City { get; set; }
        public int Pincode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public int CompanyID { get; set; }

        public static DataSet GetBranchNameAndID(int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_BranchNameAndID", objParams);
        }

        public static DataSet GetAll(int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_BRANCH_DETAILS_BY_COMPANY_ID", objParams);
        }

        public static clsBranch GetBranchByID(int branchID)
        {
            clsBranch objBranch = new clsBranch();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@BranchID", branchID);
            SqlDataReader reader = objSqlHelper.ExecuteReader("GET_BRANCH_DETAILS_BY_ID", objParams);
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    objBranch.BranchID = branchID;
                    objBranch.BranchCode = reader["BranchCode"].ToString();
                    objBranch.BranchName = reader["BranchName"].ToString();
                    objBranch.Address_line1 = reader["Address_line1"].ToString();
                    objBranch.Address_line2 = reader["Address_line2"].ToString();
                    objBranch.City = reader["City"].ToString();
                    objBranch.State = reader["State"].ToString();
                    objBranch.Country = Convert.ToString(reader["Country"]);
                    objBranch.Pincode = Convert.ToInt32(reader["Pincode"]);
                }
            }
            return objBranch;
        }


        public static int SaveBranch(clsBranch objBranch, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[11];

            objParams[0] = new SqlParameter("@BranchID", objBranch.BranchID);
            objParams[1] = new SqlParameter("@BranchCode", objBranch.BranchCode);
            objParams[2] = new SqlParameter("@BranchName", objBranch.BranchName);
            objParams[3] = new SqlParameter("@Address_line1", objBranch.Address_line1);
            objParams[4] = new SqlParameter("@City", objBranch.City);
            objParams[5] = new SqlParameter("@State", objBranch.State);
            objParams[6] = new SqlParameter("@CompanyID", objBranch.CompanyID);
            objParams[7] = new SqlParameter("@CreatedBy", userID);
            objParams[8] = new SqlParameter("@Address_line2", objBranch.Address_line2);
            objParams[9] = new SqlParameter("@Country", objBranch.Country);
            objParams[10] = new SqlParameter("@Pincode", objBranch.Pincode);


            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_BRANCH_DETAILS", objParams).ToString());
        }

        public static int RemoveBranch(int branchID, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@BranchID", branchID);
            objParams[1] = new SqlParameter("@ModifiedBy", userID);

            return int.Parse(objSqlHelper.ExecuteNonQuery("BRANCH_DETAILS_DELETE", objParams).ToString());
        }
        public static bool ValidateBranchIsExistorNot(string uName)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@BranchName", uName);
            return Convert.ToBoolean(objSqlHelper.ExecuteScalar("Validate_Branch_Exist_Or_Not", objParams).ToString());
        }

        public static bool ValidateBranchCodeIsExistorNot(string branchCode)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@BranchCode", branchCode);
            return Convert.ToBoolean(objSqlHelper.ExecuteScalar("Validate_BranchCode_Exist_Or_Not", objParams).ToString());
        }
    }
}