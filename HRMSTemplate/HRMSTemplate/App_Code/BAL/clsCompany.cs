﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsCompany
    {


        public int CompanyID { get; set; }
        [Required(ErrorMessage = "The Company Code is required.")]
        public string CompanyCode { get; set; }
        [Required(ErrorMessage = "The Company Name is required.")]
        public string CompanyName { get; set; }
        public string Tax_ID { get; set; }
        [Required]
        public string Registration_No { get; set; }
        public string GST_No { get; set; }
        public string Pan_No { get; set; }
        public string Division { get; set; }
        public string Address_line1 { get; set; }
        public string Address_line2 { get; set; }
        public string City { get; set; }
        public int Pincode { get; set; }
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Please enter a valid email address.")]
        public string EMailID { get; set; }
        [Required]
        public string ContactNo { get; set; }
        public string Country { get; set; }
        public string State { get; set; }


        public static DataSet GetAll()
        {
            SqlHelper objSqlHelper = new SqlHelper();
            return objSqlHelper.ExecuteDataSet("GET_COMPANY_DETAILS");
        }

        public static int SaveCompany(clsCompany objCompany, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[16];

            objParams[0] = new SqlParameter("@CompanyID", objCompany.CompanyID);
            objParams[1] = new SqlParameter("@CompanyCode", objCompany.CompanyCode);
            objParams[2] = new SqlParameter("@CompanyName", objCompany.CompanyName);
            //objParams[3] = new SqlParameter("@Tax_ID", objCompany.Tax_ID);
            objParams[3] = new SqlParameter("@Registration_No", objCompany.Registration_No);
            objParams[4] = new SqlParameter("@GST_No ", objCompany.GST_No);
            objParams[5] = new SqlParameter("@Pan_No", objCompany.Pan_No);
            objParams[6] = new SqlParameter("@Division", objCompany.Division);
            objParams[7] = new SqlParameter("@Address_line1", objCompany.Address_line1);
            objParams[8] = new SqlParameter("@Address_line2", objCompany.Address_line2);
            objParams[9] = new SqlParameter("@City", objCompany.City);
            objParams[10] = new SqlParameter("@Pincode", objCompany.Pincode);
            objParams[11] = new SqlParameter("@EMailID", objCompany.EMailID);
            objParams[12] = new SqlParameter("@ContactNo", objCompany.ContactNo);
            objParams[13] = new SqlParameter("@Country", objCompany.Country);
            objParams[14] = new SqlParameter("@State", objCompany.State);
            objParams[15] = new SqlParameter("@CreatedBy", userID);

            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_COMPANY_DETAILS", objParams).ToString());
        }

        public static clsCompany GetCompanyByID(int companyID)
        {
            clsCompany objCompany = new clsCompany();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            SqlDataReader reader = objSqlHelper.ExecuteReader("GET_COMPANY_DETAILS_BY_ID", objParams);
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    objCompany.CompanyID = companyID;
                    objCompany.CompanyCode = reader["CompanyCode"].ToString();
                    objCompany.CompanyName = reader["CompanyName"].ToString();
                    objCompany.Address_line1 = reader["Address_line1"].ToString();
                    objCompany.Address_line2 = reader["Address_line2"].ToString();
                    objCompany.City = reader["City"].ToString();
                    objCompany.State = reader["State"].ToString();
                    objCompany.Registration_No = reader["Registration_No"].ToString();
                    objCompany.GST_No = reader["GST_No"].ToString();
                    objCompany.Pan_No = reader["Pan_No"].ToString();
                    objCompany.Division = reader["Division"].ToString();
                    objCompany.Pincode = Convert.ToInt32(reader["Pincode"]);
                    objCompany.EMailID = reader["EMailID"].ToString();
                    objCompany.ContactNo = reader["ContactNo"].ToString();
                    objCompany.Country = reader["Country"].ToString();
                }
            }
            return objCompany;
        }

        public static int RemoveCompany(int CompanyID, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@CompanyID", CompanyID);
            objParams[1] = new SqlParameter("@ModifiedBy", userID);

            return int.Parse(objSqlHelper.ExecuteNonQuery("COMPANY_DETAILS_DELETE", objParams).ToString());
        }
        public static int ValidateLogin(string uName, string password)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@UserName", uName);
            objParams[1] = new SqlParameter("@Password", password);

            return int.Parse(objSqlHelper.ExecuteScalar("Validate_CompanyLogin", objParams).ToString());

        }
        public static bool ValidateCompanyIsExistorNot(string companyName) 
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@CompanyName", companyName);
            return Convert.ToBoolean(objSqlHelper.ExecuteScalar("Validate_Company_Exist_Or_Not", objParams).ToString());
        }
    }
}