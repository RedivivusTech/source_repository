﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsShift
    {
        public int ShiftID { get; set; }
        public string ShiftCode { get; set; }
        public string ShiftName { get; set; }
        public int CompanyID { get; set; }
        public int BranchID { get; set; }
        public TimeSpan ShiftStart { get; set; }
        public TimeSpan ShiftEnds { get; set; }
        public TimeSpan HalfDayWorkHrs { get; set; }
        public int ClientID { get; set; }

        public static DataSet GetAll(int companyID, int branchID, int clientId)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[3];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            objParams[1] = new SqlParameter("@BranchID", branchID);
            objParams[2] = new SqlParameter("@ClientID", clientId);
            return objSqlHelper.ExecuteDataSet("GET_SHIFT_DETAILS_BY_COMPANY_ID", objParams);
        }

        public static clsShift GetShiftByID(int shiftID)
        {
            clsShift objShift = new clsShift();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@ShiftID", shiftID);
            SqlDataReader reader = objSqlHelper.ExecuteReader("GET_SHIFT_DETAILS_BY_ID", objParams);
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    objShift.ShiftID = shiftID;
                    objShift.ShiftCode = reader.GetString(0);
                    objShift.ShiftName = reader.GetString(1);
                    objShift.CompanyID = reader.GetInt32(2);
                    objShift.BranchID = reader.GetInt32(3);
                    objShift.ShiftStart = reader.GetTimeSpan(4);
                    objShift.ShiftEnds = reader.GetTimeSpan(5);
                    objShift.HalfDayWorkHrs = reader.GetTimeSpan(6);
                    objShift.ClientID = reader.GetInt32(7);
                }
            }
            return objShift;
        }

        public static int SaveShift(clsShift objShift, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[10];

            objParams[0] = new SqlParameter("@ShiftID", objShift.ShiftID);
            objParams[1] = new SqlParameter("@ShiftCode", objShift.ShiftCode);
            objParams[2] = new SqlParameter("@ShiftName", objShift.ShiftName);
            objParams[3] = new SqlParameter("@CompanyID", objShift.CompanyID);
            objParams[4] = new SqlParameter("@BranchID", objShift.BranchID);
            objParams[5] = new SqlParameter("@ShiftStart", objShift.ShiftStart);
            objParams[6] = new SqlParameter("@ShiftEnd", objShift.ShiftEnds);
            objParams[7] = new SqlParameter("@CreatedBy", userID);
            objParams[8] = new SqlParameter("@HalfDayWorkHrs", objShift.HalfDayWorkHrs);
            objParams[9] = new SqlParameter("@ClientID", objShift.ClientID);

            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_SHIFT_DETAILS", objParams).ToString());
        }

        public static int RemoveShift(int shiftID, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@ShiftID", shiftID);
            objParams[1] = new SqlParameter("@ModifiedBy", userID);

            return int.Parse(objSqlHelper.ExecuteNonQuery("SHIFT_DETAILS_DELETE", objParams).ToString());
        }

    }
}