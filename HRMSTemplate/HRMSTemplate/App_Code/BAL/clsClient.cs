﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsClient
    {
        public int ClientID { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string Address_line1 { get; set; }
        public string Address_line2 { get; set; }
        public string City { get; set; }
        public int Pincode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public int NoOfEmployee { get; set; }
        public int ParentClientID { get; set; }
        public int CompanyID { get; set; }
        public int BranchID { get; set; }
        public string RegistrationNo { get; set; }
        public string Division { get; set; }
        public string PANNo { get; set; }
        public string GSTNo { get; set; }
        public string ContactNo { get; set; }
        public string EmailID { get; set; }

        public static DataSet GetAll(int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_CLIENT_DETAILS_BY_COMPANY_ID", objParams);
        }

        public static clsClient GetClientByID(int clientID)
        {
            clsClient objClient = new clsClient();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@ClientID", clientID);
            SqlDataReader reader = objSqlHelper.ExecuteReader("GET_CLIENT_DETAILS_BY_ID", objParams);
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    objClient.ClientID = clientID;
                    objClient.ClientCode = reader["ClientCode"].ToString();
                    objClient.ClientName = reader["ClientName"].ToString();
                    objClient.Address_line1 = reader["Address_line1"].ToString();
                    objClient.Address_line2 = reader["Address_line2"].ToString();
                    objClient.City = reader["City"].ToString();
                    objClient.State = reader["State"].ToString();
                    objClient.ParentClientID = (int)reader["ParentClientID"];
                    objClient.NoOfEmployee = (int)(reader["No_of_Employees"]);
                    objClient.BranchID = Convert.ToInt32(reader["BranchID"]);
                    objClient.RegistrationNo = reader["Registration_No"].ToString();
                    objClient.GSTNo = reader["GST_No"].ToString();
                    objClient.PANNo = reader["Pan_No"].ToString();
                    objClient.Division = reader["Division"].ToString();
                    objClient.Pincode = Convert.ToInt32(reader["Pincode"]);
                    objClient.EmailID = reader["EMailID"].ToString();
                    objClient.ContactNo = reader["ContactNo"].ToString();
                    objClient.Country = reader["Country"].ToString();
                }
            }
            return objClient;
        }


        public static int SaveClient(clsClient objClient, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[20];

            objParams[0] = new SqlParameter("@ClientID", objClient.ClientID);
            objParams[1] = new SqlParameter("@ClientCode", objClient.ClientCode);
            objParams[2] = new SqlParameter("@ClientName", objClient.ClientName);
            objParams[3] = new SqlParameter("@Address_line1", objClient.Address_line1);
            objParams[4] = new SqlParameter("@City", objClient.City);
            objParams[5] = new SqlParameter("@State", objClient.State);
            objParams[6] = new SqlParameter("@CompanyID", objClient.CompanyID);
            objParams[7] = new SqlParameter("@CreatedBy", userID);
            objParams[8] = new SqlParameter("@Address_line2", objClient.Address_line2);
            objParams[9] = new SqlParameter("@ParentClientID", objClient.ParentClientID);
            objParams[10] = new SqlParameter("@NoOfEmp", objClient.NoOfEmployee);
            objParams[11] = new SqlParameter("@BranchID", objClient.BranchID);
            objParams[12] = new SqlParameter("@RegNo", objClient.RegistrationNo);
            objParams[13] = new SqlParameter("@PANNo", objClient.PANNo);
            objParams[14] = new SqlParameter("@GSTNo", objClient.GSTNo);
            objParams[15] = new SqlParameter("@Division", objClient.Division);
            objParams[16] = new SqlParameter("@Pincode", objClient.Pincode);
            objParams[17] = new SqlParameter("@EmailID", objClient.EmailID);
            objParams[18] = new SqlParameter("@ContactNo", objClient.ContactNo);
            objParams[19] = new SqlParameter("@Country", objClient.Country);


            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_CLIENT_DETAILS", objParams).ToString());
        }

        public static int RemoveClient(int clientID, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@ClientID", clientID);
            objParams[1] = new SqlParameter("@ModifiedBy", userID);

            return int.Parse(objSqlHelper.ExecuteNonQuery("CLIENT_DETAILS_DELETE", objParams).ToString());
        }

        public static DataSet GetClientNameAndID(int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_ClientNameAndID", objParams);
        }

        public static DataSet GetClientNameAndID(int companyID, int branchID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            objParams[1] = new SqlParameter("@BranchID", branchID);
            return objSqlHelper.ExecuteDataSet("GET_ClientNameAndID_For_Global", objParams);
        }
        public static bool ValidateClientIsExistorNot(string uName) 
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@ClientName", uName);
            return Convert.ToBoolean(objSqlHelper.ExecuteScalar("Validate_Client_Exist_Or_Not", objParams).ToString());
        }
    }
}