﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsUserRolePermission
    {
        public int UserRoleId { get; set; }
        public int PermissionID { get; set; }
        public int Modules_ID { get; set; }
        public bool IsActive { get; set; }
        public int CompanyID { get; set; }


        public static DataSet GetPermissions()
        {
            SqlHelper objSqlHelper = new SqlHelper();
            return objSqlHelper.ExecuteDataSet("GET_PERMISSIONS");
        }

        public static int SaveUserRolePermission(clsUserRolePermission objUserRolePermission, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();

            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@UserRoleID", objUserRolePermission.UserRoleId);
            objParams[1] = new SqlParameter("@ModuleID", objUserRolePermission.Modules_ID);
            objParams[2] = new SqlParameter("@CompanyID", objUserRolePermission.CompanyID);
            objParams[3] = new SqlParameter("@CreatedBy", userID);
            objParams[4] = new SqlParameter("@IsActive", objUserRolePermission.IsActive);
            objParams[5] = new SqlParameter("@PermissionID", objUserRolePermission.PermissionID);
            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_USER_ROLE_PERMISSION", objParams).ToString());
        }

        public static DataTable GetByUserPermissionByID(clsUserRolePermission rolePermission)
        {
            clsUserRolePermission objUserPermission = new clsUserRolePermission();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[3];

            objParams[0] = new SqlParameter("@UserRoleID", rolePermission.UserRoleId);
            objParams[1] = new SqlParameter("@CompanyID", rolePermission.CompanyID);
            objParams[2] = new SqlParameter("@ModuleID", rolePermission.Modules_ID);
            return objSqlHelper.ExecuteDataTable("GET_USER_ROLE_PERMISSION_ACCESS_BY_ID", objParams);

        }
    }
}