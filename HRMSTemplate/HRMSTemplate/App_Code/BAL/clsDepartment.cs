﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsDepartment
    {
        public int DepartmentID { get; set; }
        public int ParentDeptID { get; set; }
        [Required(ErrorMessage = "The Department Code is Required.")]
        public string DepartmentCode { get; set; }
        [Required(ErrorMessage = "The Department Name is Required.")]
        public string DepartmentName { get; set; }
        public int CompanyID { get; set; }

        public static DataSet GetAll(int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_DEPT_DETAILS_BY_COMPANY_ID", objParams);
        }
        public static clsDepartment GetDepartmentByID(int deptID)
        {
            clsDepartment objDept = new clsDepartment();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@DepartmentID", deptID);
            SqlDataReader reader = objSqlHelper.ExecuteReader("GET_DEPARTMENT_DETAILS_BY_ID", objParams);
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    objDept.DepartmentID = deptID;
                    objDept.DepartmentCode = reader["DepartmentCode"].ToString();
                    objDept.DepartmentName = reader["DepartmentName"].ToString();
                    objDept.CompanyID = Convert.ToInt32(reader["CompanyID"]);
                    objDept.ParentDeptID = Convert.ToInt32(reader["ParentDeptID"]);
                }
            }
            return objDept;
        }
        public static int SaveDepartment(clsDepartment objDept, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[6];

            objParams[0] = new SqlParameter("@DepartmentID", objDept.DepartmentID);
            objParams[1] = new SqlParameter("@DepartmentCode", objDept.DepartmentCode);
            objParams[2] = new SqlParameter("@DepartmentName", objDept.DepartmentName);
            objParams[3] = new SqlParameter("@ParentDeptID", objDept.ParentDeptID);
            objParams[4] = new SqlParameter("@CompanyID", objDept.CompanyID);
            objParams[5] = new SqlParameter("@CreatedBy", userID);


            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_DEPARMENT_DETAILS", objParams).ToString());
        }

        public static int RemoveDepartment(int deptID, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@DepartmentID", deptID);
            objParams[1] = new SqlParameter("@ModifiedBy", userID);

            return int.Parse(objSqlHelper.ExecuteNonQuery("DEPARTMENT_DETAILS_DELETE", objParams).ToString());
        }

        public static DataSet GetDepartmentNameAndID(int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_DepartmentNameAndID", objParams);
        }
        public static bool ValidateDepartmentIsExistorNot(string deptName) 
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@DepartmentName", deptName);
            return Convert.ToBoolean(objSqlHelper.ExecuteScalar("Validate_Department_Exist_Or_Not", objParams).ToString());
        }
        public static bool ValidateDepartmenCodetIsExistorNot(string deptCode) 
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@DepartmentCode", deptCode);
            return Convert.ToBoolean(objSqlHelper.ExecuteScalar("Validate_DepartmentCode_Exist_Or_Not", objParams).ToString());
        }
    }
}