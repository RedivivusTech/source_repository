﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsLeave
    {
        public int LeaveID { get; set; }
        public string LeaveName { get; set; }
        public string ShortName { get; set; }
        public int NoOfLeaves { get; set; }
        public int ClientID { get; set; }
        public int LeaveYearId { get; set; }
        public bool IsCarryForward { get; set; }
        public int CompanyID { get; set; }
        public string PaymentRatio { get; set; }

        public static DataSet GetAll(int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_LEAVES_DETAILS", objParams);
        }

        public static clsLeave GetLeavesByID(int leaveID)
        {
            clsLeave objLeave = new clsLeave();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@LeaveID", leaveID);
            SqlDataReader reader = objSqlHelper.ExecuteReader("GET_LEAVE_DETAILS_BY_ID", objParams);
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    objLeave.LeaveID = Convert.ToInt32(reader["LeaveMID"]);
                    objLeave.LeaveName = reader["LeaveName"].ToString();
                    objLeave.ShortName = reader["ShortName"].ToString();
                    objLeave.NoOfLeaves = Convert.ToInt32(reader["NoOfLeaves"]);
                    objLeave.LeaveYearId = Convert.ToInt32(reader["LeaveYearId"]);
                    objLeave.ClientID = Convert.ToInt32(reader["ClientID"]);
                    objLeave.PaymentRatio = reader["PaymentRatio"].ToString();
                }
            }
            return objLeave;
        }

        public static int SaveLeaves(clsLeave objLeaves, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[9];

            objParams[0] = new SqlParameter("@LeaveID", objLeaves.LeaveID);
            objParams[1] = new SqlParameter("@LeaveName", objLeaves.LeaveName);
            objParams[2] = new SqlParameter("@ShortName", objLeaves.ShortName);
            objParams[3] = new SqlParameter("@NoOfLeaves", objLeaves.NoOfLeaves);
            objParams[4] = new SqlParameter("@LeaveYearId", objLeaves.LeaveYearId);
            objParams[5] = new SqlParameter("@ClientID", objLeaves.ClientID);
            objParams[6] = new SqlParameter("@CreatedBy", userID);
            objParams[7] = new SqlParameter("@CompanyID", objLeaves.CompanyID);
            objParams[8] = new SqlParameter("@PaymentRatio", objLeaves.PaymentRatio);


            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_LEAVES_DETAILS", objParams).ToString());
        }

        public static int RemoveLeaves(int leaveID, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@LeaveID", leaveID);
            objParams[1] = new SqlParameter("@ModifiedBy", userID);

            return int.Parse(objSqlHelper.ExecuteNonQuery("LEAVE_DETAILS_DELETE", objParams).ToString());
        }

        public static DataSet GetLeaveNameAndID(int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_LeaveNameAndID", objParams);
        }
    }
}