﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsUserModuleAccess
    {
        public int UserID { get; set; }
        public int Modules_ID { get; set; }
        public bool IsActive { get; set; }
        public int CompanyID { get; set; }


        public static DataTable GetByUserModuleByID(int userID, int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@UserID", userID);
            objParams[1] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataTable("GET_USER_MODULE_ACCESS_BY_ID", objParams);
        }

        public static int UpdateUserModuleAccess(clsUserModuleAccess objUserModuleAccess, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();

            SqlParameter[] objParams = new SqlParameter[5];
            objParams[0] = new SqlParameter("@UserID", objUserModuleAccess.UserID);
            objParams[1] = new SqlParameter("@ModuleID", objUserModuleAccess.Modules_ID);
            objParams[2] = new SqlParameter("@CompanyID", objUserModuleAccess.CompanyID);
            objParams[3] = new SqlParameter("@CreatedBy", userID);
            objParams[4] = new SqlParameter("@IsActive", objUserModuleAccess.IsActive);
            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_USER_MODULE_ACCESS", objParams).ToString());
        }

        public static int CheckModuleAccessForPermission(int userID, int modulesID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@UserID", userID);
            objParams[1] = new SqlParameter("@ModuleID", modulesID);
            return int.Parse(objSqlHelper.ExecuteScalar("CHECK_USER_MODULE_ACCESS_FOR_PERMISSION", objParams).ToString());
        }
        public static DataTable GetAllModulesByUserIDAndUserRoleID(int userID,int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@UserID", 4);
            objParams[1] = new SqlParameter("@CompanyID", 1);
            return objSqlHelper.ExecuteDataTable("GET_USER_MENUS_ACCESS_BY_USER_ID", objParams);
        }
    }
}