﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsUserPermission
    {
        public int UserID { get; set; }
        public int PermissionID { get; set; }
        public int ModulesID { get; set; }
        public bool IsActive { get; set; }
        public int CompanyID { get; set; }


        public static DataTable GetByUserPermissionByID(clsUserPermission userPermission)
        {
            clsUserPermission objUserPermission = new clsUserPermission();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[3];

            objParams[0] = new SqlParameter("@UserID", userPermission.UserID);
            objParams[1] = new SqlParameter("@CompanyID", userPermission.CompanyID);
            objParams[2] = new SqlParameter("@ModuleID", userPermission.ModulesID);
            return objSqlHelper.ExecuteDataTable("GET_USER_PERMISSION_ACCESS_BY_ID", objParams);

        }

        public static int UpdateUserPermission(clsUserPermission objUserPermission, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();

            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@UserID", objUserPermission.UserID);
            objParams[1] = new SqlParameter("@ModuleID", objUserPermission.ModulesID);
            objParams[2] = new SqlParameter("@CompanyID", objUserPermission.CompanyID);
            objParams[3] = new SqlParameter("@CreatedBy", userID);
            objParams[4] = new SqlParameter("@IsActive", objUserPermission.IsActive);
            objParams[5] = new SqlParameter("@PermissionID", objUserPermission.PermissionID);
            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_USER_PERMISSION", objParams).ToString());
        }
    }
}