﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsFinancialYear
    {
        public int ID { get; set; }
        public string FinancialYear { get; set; }
        public DateTime YearStart { get; set; }
        public DateTime YearEnd { get; set; }

        public static DataSet GetAll()
        {
            SqlHelper objSqlHelper = new SqlHelper();
            return objSqlHelper.ExecuteDataSet("GET_FINANCIAL_DETAILS");
        }

        public static clsFinancialYear GetByID(int ID)
        {
            clsFinancialYear objFinancial = new clsFinancialYear();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@ID", ID);
            SqlDataReader reader = objSqlHelper.ExecuteReader("GET_FINANCIAL_DETAILS_BY_ID", objParams);
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    objFinancial.ID = ID;
                    objFinancial.FinancialYear = reader["FinancialYear"].ToString();
                    objFinancial.YearStart = Convert.ToDateTime(reader["YearStart"]);
                    objFinancial.YearEnd = Convert.ToDateTime(reader["YearEnd"]);
                }
            }
            return objFinancial;
        }

        public static DataSet GetYearNameAndID(int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_YearNameAndID", objParams);
        }

        public static int SaveFinancialYear(clsFinancialYear objYear, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[5];

            objParams[0] = new SqlParameter("@ID", objYear.ID);
            objParams[1] = new SqlParameter("@FinancialYear", objYear.FinancialYear);
            objParams[2] = new SqlParameter("@YearStart", objYear.YearStart);
            objParams[3] = new SqlParameter("@YearEnd", objYear.YearEnd);
            objParams[4] = new SqlParameter("@CreatedBy", userID);
            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_FINANCIAL_YEAR_DETAILS", objParams).ToString());
        }

        public static int RemoveFinancialYear(int ID, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@ID", ID);
            objParams[1] = new SqlParameter("@ModifiedBy", userID);

            return int.Parse(objSqlHelper.ExecuteNonQuery("FINANCIAL_DETAILS_DELETE", objParams).ToString());
        }
    }
}