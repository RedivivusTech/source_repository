﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsHoliday
    {
        public int ID { get; set; }
        [Required(ErrorMessage ="The Holiday Date is required.")]
        [DataType(DataType.Date)]
        public DateTime HolidayDate { get; set; }
        [Required(ErrorMessage ="The Holiday Name is required.")]
        public string HolidayName { get; set; }
        [Required(ErrorMessage = "The Financial Year is required")]
        public int FinancialYear { get; set; }
        public int ClientID { get; set; }
        public int CompanyID { get; set; }

        public static DataSet GetAll(int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_HOLIDAY_DETAILS_BY_COMPANY_ID", objParams);
        }

        public static clsHoliday GetHolidayByID(int ID)
        {
            clsHoliday objHoliday = new clsHoliday();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@ID", ID);
            SqlDataReader reader = objSqlHelper.ExecuteReader("GET_HOLIDAY_DETAILS_BY_ID", objParams);
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    objHoliday.ID = (int)reader["ID"];
                    objHoliday.HolidayName = reader["HolidayName"].ToString();
                    objHoliday.HolidayDate = Convert.ToDateTime(reader["Holiday"]);
                    objHoliday.FinancialYear = Convert.ToInt32(reader["FinYear"]);
                    objHoliday.ClientID = Convert.ToInt32(reader["ClientID"]);
                }
            }
            return objHoliday;
        }

        public static bool ValidateHolidayNameIsExistorNot(string holidayName)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@HolidayName", holidayName);
            return Convert.ToBoolean(objSqlHelper.ExecuteScalar("Validate_Holiday_Exist_Or_Not", objParams).ToString());
        }

        public static int SaveHoliday(clsHoliday objHoliday, int usetID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[7];

            objParams[0] = new SqlParameter("@ID", objHoliday.ID);
            objParams[1] = new SqlParameter("@Holiday", objHoliday.HolidayDate);
            objParams[2] = new SqlParameter("@HolidayName", objHoliday.HolidayName);
            objParams[3] = new SqlParameter("@FinancialYear", objHoliday.FinancialYear);
            objParams[4] = new SqlParameter("@CreatedBy", usetID);
            objParams[5] = new SqlParameter("@ClientID", objHoliday.ClientID);
            objParams[6] = new SqlParameter("@CompanyID", objHoliday.CompanyID);

            return int.Parse(objSqlHelper.ExecuteNonQuery("Save_tbl_HolidayMaster", objParams).ToString());
        }

        public static int RemoveHoliday(int ID, int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[2];

            objParams[0] = new SqlParameter("@ID", ID);
            objParams[1] = new SqlParameter("@ModifiedBy", userID);

            return int.Parse(objSqlHelper.ExecuteNonQuery("HOLIDAY_DETAILS_DELETE", objParams).ToString());
        }
    }
}