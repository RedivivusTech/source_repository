﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsUserRole
    {
        public static DataSet GetParentRoleNameAndID()
        {
            SqlHelper sqlHelper = new SqlHelper();
            return sqlHelper.ExecuteDataSet("GET_ParentRoleNameAndID");
        }

        public static DataTable GetAll(int parentRoleID)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@ParentRoleID", parentRoleID);
            return sqlHelper.ExecuteDataTable("GET_ROLES_BY_PARENT_ID", objParams);
        }

        public static DataTable GetAllModules()
        {
            SqlHelper sqlHelper = new SqlHelper();
            return sqlHelper.ExecuteDataTable("GET_ALL_MODULES");
        }
    }
}