﻿using HRMSTemplate.App_Code.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HRMSTemplate.App_Code.BAL
{
    public class clsUser
    {
        public int UserID { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public int UserRoleID { get; set; }
        public int ParentRoleID { get; set; }
        public int CompanyID { get; set; }
        public int ClientID { get; set; }
        public int AssociateID { get; set; }
        public int EmployeeID { get; set; }
        public static DataSet GetAll(int companyID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@CompanyID", companyID);
            return objSqlHelper.ExecuteDataSet("GET_USER_DETAILS_BY_COMPANY_ID", objParams);
        }

        public static clsUser GetUserByID(int userID)
        {
            clsUser objUser = new clsUser();
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@UserID", userID);
            SqlDataReader reader = objSqlHelper.ExecuteReader("GET_USER_DETAILS_BY_ID", objParams);
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    objUser.UserID = userID;
                    objUser.LoginName = reader["LoginName"].ToString();
                    objUser.Password = reader["Password"].ToString();
                    objUser.ParentRoleID = Convert.ToInt32(reader["ParentRoleID"]);
                    objUser.UserRoleID = Convert.ToInt32(reader["UserRoleID"]);
                    //objUser.CompanyID = (int)reader["CompanyID"];
                    objUser.ClientID = Convert.ToInt32(reader["ClientID"]);
                    objUser.AssociateID = Convert.ToInt32(reader["AssociateID"]);
                }
            }
            return objUser;
        }
        public static int SaveUser(clsUser objUser, int userid)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[9];

            objParams[0] = new SqlParameter("@UserID", objUser.UserID);
            objParams[1] = new SqlParameter("@LoginName", objUser.LoginName);
            objParams[2] = new SqlParameter("@Password", objUser.Password);
            objParams[3] = new SqlParameter("@ParentRoleID", objUser.ParentRoleID);
            objParams[4] = new SqlParameter("@UserRoleID", objUser.UserRoleID);
            objParams[5] = new SqlParameter("@CompanyID", objUser.CompanyID);
            objParams[6] = new SqlParameter("@ClientID", objUser.ClientID);
            objParams[7] = new SqlParameter("@AssociateID", objUser.AssociateID);
            objParams[8] = new SqlParameter("@CreatedBy", userid);

            return int.Parse(objSqlHelper.ExecuteNonQuery("SAVE_USER_DETAILS", objParams).ToString());
        }

        public static int RemoveUser(int userID)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];

            objParams[0] = new SqlParameter("@UserID", userID);
            return int.Parse(objSqlHelper.ExecuteNonQuery("USER_DETAILS_DELETE", objParams).ToString());
        }

        public static bool ValidateUserIsExistorNot(string uName)
        {
            SqlHelper objSqlHelper = new SqlHelper();
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@UserName", uName);
            return Convert.ToBoolean(objSqlHelper.ExecuteScalar("Validate_User_Exist_Or_Not", objParams).ToString());
        }
         
    }
}